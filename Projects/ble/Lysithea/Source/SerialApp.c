#include <stdio.h>
#include <string.h>
#include "bcomdef.h"
#include "OSAL.h"
#include "OSAL_PwrMgr.h"

#include "OnBoard.h"
#include "hal_uart.h"
#include "hal_lcd.h"
#include "keyfobdemo.h"
#include "md_uart_profile.h"
//#define DEBUG_SERIAL
#include "SerialApp.h"


static uint8 sendMsgTo_TaskID;
/* Ative delay: 125 cycles ~1 msec */
#define KFD_HAL_DELAY(n) st( { volatile uint32 i; for (i=0; i<(n); i++) { }; } )

void SerialApp_InitEx( serialCBack_t npiCBack, uint8 baudrate, uint8 parity, uint8 stopbit)
{
  halUARTCfg_t uartConfig;

  // configure UART
  uartConfig.configured           = TRUE;
  uartConfig.baudRate             = baudrate;//NPI_UART_BR;
  uartConfig.parity               = parity;
  uartConfig.stopbit              = stopbit;
  uartConfig.flowControl          = SBP_UART_FC;
  uartConfig.flowControlThreshold = SBP_UART_FC_THRESHOLD;
  uartConfig.rx.maxBufSize        = SBP_UART_RX_BUF_SIZE;
  uartConfig.tx.maxBufSize        = SBP_UART_TX_BUF_SIZE;
  uartConfig.idleTimeout          = SBP_UART_IDLE_TIMEOUT;
  uartConfig.intEnable            = SBP_UART_INT_ENABLE;
  uartConfig.callBackFunc         = (halUARTCBack_t)npiCBack;

  // start UART
  // Note: Assumes no issue opening UART port.
  (void)HalUARTOpen( SBP_UART_PORT, &uartConfig );

  return;
}

/*
串口设备初始化，
必须在使用串口打印之前调用该函数进行uart初始化
*/
void SerialApp_Init( uint8 taskID , uint8 mode, uint8 baudRate)
{
    HalUARTInit();
    //调用uart初始化代码
    serialAppInitTransport(mode, baudRate);
    //记录任务函数的taskID，备用
    sendMsgTo_TaskID = taskID;
}

/*
uart初始化代码，配置串口的波特率、流控制等
*/
void serialAppInitTransport(uint8 mode, uint8 baudRate)
{
    halUARTCfg_t uartConfig;

    // configure UART
    uartConfig.configured           = TRUE;
    uartConfig.baudRate             = baudRate;//波特率
    uartConfig.flowControl          = SBP_UART_FC;//流控制
    uartConfig.flowControlThreshold = SBP_UART_FC_THRESHOLD;//流控制阈值，当开启flowControl时，该设置有效
    uartConfig.rx.maxBufSize        = SBP_UART_RX_BUF_SIZE;//uart接收缓冲区大小
    uartConfig.tx.maxBufSize        = SBP_UART_TX_BUF_SIZE;//uart发送缓冲区大小
    uartConfig.idleTimeout          = SBP_UART_IDLE_TIMEOUT;
    uartConfig.intEnable            = SBP_UART_INT_ENABLE;//是否开启中断
    //uartConfig.callBackFunc         = sbpSerialAppCallback;//uart接收回调函数，在该函数中读取可用uart数据
    if (mode == 0)
    {
        uartConfig.callBackFunc         = NULL;
    }
    else//transport mode.
    {
        //uartConfig.callBackFunc         = sbpSerialAppCallbackTrans;
        uartConfig.callBackFunc         = NULL;
    }

    // start UART
    // Note: Assumes no issue opening UART port.
    (void)HalUARTOpen( SBP_UART_PORT, &uartConfig );

    return;
}

uint16 numBytes;
//uint8 g_uart_cb_event;
/*
uart接收回调函数
当我们通过pc向开发板发送数据时，会调用该函数来接收
*/
void sbpSerialAppCallback(uint8 port, uint8 event)
{
    //uint8 pktBuffer[SBP_UART_RX_BUF_SIZE] = {0};
    //g_uart_cb_event = event;
    // unused input parameter; PC-Lint error 715.
    //(void)event;
    //HalLcdWriteString("Data form my UART:", HAL_LCD_LINE_4 );
    //返回可读的字节
    #if 0
    if ( (numBytes = Hal_UART_RxBufLen(port)) > 0 )
    {
        //读取全部有效的数据，这里可以一个一个读取，以解析特定的命令
        (void)HalUARTRead (port, pktBuffer, numBytes);
        //HalLcdWriteString(pktBuffer, HAL_LCD_LINE_5 );
        osal_set_event(sendMsgTo_TaskID, KFD_UART_EVT);
    }
    #else
    if (event & (HAL_UART_RX_TIMEOUT | HAL_UART_RX_ABOUT_FULL | HAL_UART_RX_FULL))
    {
        #if 0
        numBytes = HalUARTRead (port, pktBuffer, SBP_UART_RX_BUF_SIZE);
        
        //SerialPrintString("UART read : ");
        HalUARTWrite (SBP_UART_PORT, pktBuffer, numBytes);
        //SerialPrintString("\r\n");
        //osal_set_event(sendMsgTo_TaskID, KFD_UART_EVT);
        osal_start_timerEx( sendMsgTo_TaskID, KFD_UART_EVT, 100 );
        #else
        //osal_start_timerEx( sendMsgTo_TaskID, KFD_UART_EVT, 50 );
        #endif
    }
    if (event & HAL_UART_TX_FULL)
    {
        P1_0 = 1; // Green LED.
    }
    #endif
}

void sbpSerialAppCallbackTrans(uint8 port, uint8 event)
{
    //static unsigned count=0;
  uint8  pktBuffer[SBP_UART_RX_BUF_SIZE] = {0};
  // unused input parameter; PC-Lint error 715.
 // (void)event;
 #if 0
  //返回可读的字节
  if ( (numBytes = Hal_UART_RxBufLen(port)) > 0 ){
  	//读取全部有效的数据，这里可以一个一个读取，以解析特定的命令
	(void)HalUARTRead (port, pktBuffer, numBytes);
	if (FALSE == MDUARTSerialAppSendNoti(pktBuffer,numBytes))
	{
        sprintf(pktBuffer, "Please connect me first!!!\r\n");
        HalUARTWrite (SBP_UART_PORT, pktBuffer, strlen(pktBuffer));
	}
  }
  #elif 0
  if (event & (HAL_UART_RX_TIMEOUT | HAL_UART_RX_ABOUT_FULL | HAL_UART_RX_FULL))
  {
      uint16 len = HalUARTRead (port, pktBuffer, SBP_UART_RX_BUF_SIZE);
      if (FALSE == MDUARTSerialAppSendNoti(pktBuffer,len))
    	{
            if (len != 0)
            {
                sprintf(pktBuffer, "Please connect me first!!!\r\n");
                HalUARTWrite (SBP_UART_PORT, pktBuffer, strlen(pktBuffer));
            }
    	}
  }
  #elif 0
  if (event & (HAL_UART_RX_TIMEOUT | HAL_UART_RX_FULL))
  {
      static uint32 old_time = 0;     //老时间
      uint32 new_time;            //新时间

      new_time = osal_GetSystemClock();
      if (new_time - old_time <= 300)
      {
        //old_time = new_time;
        return;
      }
      old_time = new_time;
      //osal_stop_timerEx(sendMsgTo_TaskID, KFD_UART_EVT);
//      osal_start_timerEx( sendMsgTo_TaskID, KFD_UART_EVT, 100 );

      if (Hal_UART_RxBufLen(port) > 0)
      {
        osal_set_event(sendMsgTo_TaskID, KFD_UART_EVT);
      }
  }
  #elif 1
    static uint32 old_time;     //老时间
    static uint32 old_time_data_len = 0;     //老时间是的数据长度    
    uint32 new_time;            //新时间
    bool ret;
    uint8 readMaxBytes = 50;
        
    if (event & (HAL_UART_RX_TIMEOUT | HAL_UART_RX_FULL))   //串口有数据
    {
        uint8 numBytes = 0;

        numBytes = Hal_UART_RxBufLen(port);           //读出串口缓冲区有多少字节
        
        if(numBytes == 0)
        {
            //LCD_WRITE_STRING_VALUE( "ERROR: numBytes=", numBytes, 10, HAL_LCD_LINE_1 );
            old_time_data_len = 0;
            return;
        }
        if(old_time_data_len == 0)
        {
            old_time = osal_GetSystemClock(); //有数据来时， 记录一下
            old_time_data_len = numBytes;
        }
        else
        {   
            new_time = osal_GetSystemClock(); //当前时间
            
            if( (numBytes >= readMaxBytes) 
                || ( (new_time - old_time) > 20/*ms*/))
            {
                uint8 sendBytes = 0;
                uint8 *buffer = osal_mem_alloc(readMaxBytes);

                if(!buffer)
                {
                    HalUARTWrite (SBP_UART_PORT, "FAIL", 4);
                    return;
                }
                
                if(numBytes > readMaxBytes)
                {
                    sendBytes = readMaxBytes;
                }
                else
                {
                    sendBytes = numBytes;
                }

                //if(!simpleBLE_IfConnected())
                {
                    //numBytes = NpiReadBuffer(buf, sizeof(buf));
                    //NpiClearBuffer();
                     //NPI_ReadTransport(buffer,sendBytes);    //释放串口数据    
                    uint16 len = HalUARTRead (port, pktBuffer, SBP_UART_RX_BUF_SIZE);

                    if (FALSE == MDUARTSerialAppSendNoti(pktBuffer,len))
                    {
                        sprintf(buffer, "Please connect me first!!!\r\n");
                        HalUARTWrite (SBP_UART_PORT, buffer, strlen(buffer));
                    }
                }
                

                old_time = new_time;
                old_time_data_len = numBytes - sendBytes;


                osal_mem_free(buffer);
            }                
        }    
    }
  #endif
}

/*
打印一段数据
pBuffer可以包含0x00
*/
void sbpSerialAppWrite(uint8 *pBuffer, uint16 length)
{
    HalUARTWrite (SBP_UART_PORT, pBuffer, length);
}



#if defined(DEBUG_SERIAL)
/*
打印一个字符串
str不可以包含0x00，除非结尾
*/
void SerialPrintString(uint8 str[])
{
    HalUARTWrite (SBP_UART_PORT, str, osal_strlen((char *)str));
    KFD_HAL_DELAY(6250);
}

/*
打印指定的格式的数值
参数
title,前缀字符串
value,需要显示的数值
format,需要显示的进制，十进制为10,十六进制为16
*/

void SerialPrintValue(char *title, uint16 value, uint8 format)
{
    uint8 tmpLen;
    //uint8 buf[256];
    uint32 err;
    //uint8 pktBuffer[256] = {0};
    uint8 pktBuffer[150] = {0};
    
    tmpLen = (uint8)osal_strlen( (char *)title );
    osal_memcpy( pktBuffer, title, tmpLen );
    pktBuffer[tmpLen] = ' ';
    err = (uint32)(value);
    _ltoa( err, &pktBuffer[tmpLen + 1], format );
    strcat((char*)pktBuffer, "\r\n");
    SerialPrintString(pktBuffer);
}
#endif
