#include <stdio.h>
#include "bcomdef.h"
#include "OSAL.h"
#include "OSAL_PwrMgr.h"
#include "OnBoard.h"
#include "hal_led.h"
#include "hal_key.h"
#include "hal_lcd.h"
#include "gatt.h"
#include "ll.h"
#include "hci.h"
#include "gapgattserver.h"
#include "gattservapp.h"
#include "central.h"
#include "peripheral.h"
#include "gapbondmgr.h"
#include "simpleGATTprofile.h"
//#include "npi.h"
#include "osal_snv.h"
#include "simpleBLE.h"
#include "SerialApp.h"

bool simpleBLEChar6DoWrite2 = TRUE;
#if 1
SYS_CONFIG sys_config;

extern gaprole_States_t gapProfileState;   // 从机连接状态

BLE_CENTRAL_CONNECT_CMD g_Central_connect_cmd  = BLE_CENTRAL_CONNECT_CMD_NULL;

static void simpleBLE_NpiSerialCallback( uint8 port, uint8 events );
static void simpleBLE_UartDataMain(uint8 *buf, uint8 numBytes);
static bool simpleBLE_AT_CMD_Handle(uint8 *pBuffer, uint16 length);
static void simpleBLE_SendMyData_ForTest();
#endif

#if 1
// 该函数延时时间为1ms， 用示波器测量过， 稍有误差， 但误差很小  --amomcu.com
void simpleBLE_Delay_1ms(int times)
{
  while(times--)
  {
      int i=0;
      for(i=1500;i>0;i--)
      {
    	  asm("nop");
      }
  }
}

// 字符串对比
static uint8 str_cmp(uint8 *p1,uint8 *p2,uint8 len)
{
  uint8 i=0;
  while(i<len){
    if(p1[i]!=p2[i])
      return 0;
    i++;
  }
  return 1;
}
// 字符串转数字
uint32 str2Num(uint8* numStr, uint8 iLength)
{
    uint8 i = 0;
    int32 rtnInt = 0;
 
    /* 
          为代码简单，在确定输入的字符串都是数字的
          情况下，此处未做检查，否则要检查
          numStr[i] - '0'是否在[0, 9]这个区间内
    */
    for(; i < iLength && numStr[i] != '\0'; ++i)
        rtnInt = rtnInt * 10 + (numStr[i] - '0');    
 
    return rtnInt;
}

/*********************************************************************
 * @fn      bdAddr2Str
 *
 * @brief   Convert Bluetooth address to string
 *
 * @return  none
 */
char *bdAddr2Str( uint8 *pAddr )
{
#define B_ADDR_STR_LEN                        15

  uint8       i;
  char        hex[] = "0123456789ABCDEF";
  static char str[B_ADDR_STR_LEN];
  char        *pStr = str;
  
  *pStr++ = '0';
  *pStr++ = 'x';
  
  // Start from end of addr
  pAddr += B_ADDR_LEN;
  
  for ( i = B_ADDR_LEN; i > 0; i-- )
  {
    *pStr++ = hex[*--pAddr >> 4];
    *pStr++ = hex[*pAddr & 0x0F];
  }
  
  *pStr = 0;
  
  return str;
}
#endif

// 保存所有数据到nv flash
void simpleBLE_WriteAllDataToFlash()
{   // 写所有参数
    osal_snv_write(0x80, sizeof(SYS_CONFIG), &sys_config); 
}

// 读取自定义的 nv flash 数据  -------未使用到
void simpleBLE_ReadAllDataToFlash()
{
    int8 ret8 = osal_snv_read(0x80, sizeof(SYS_CONFIG), &sys_config);
}

//flag: PARA_ALL_FACTORY:  全部恢复出厂设置
//flag: PARA_PARI_FACTORY: 清除配对信息
void simpleBLE_SetAllParaDefault(PARA_SET_FACTORY flag)    
{
    if(flag == PARA_ALL_FACTORY)
    {
        sys_config.baudrate = HAL_UART_BR_9600;  
        //sys_config.baudrate = HAL_UART_BR_115200;  
        sys_config.parity = 0;  
        sys_config.stopbit = 0;  

        sys_config.mode = BLE_MODE_SERIAL;         //工作模式 0:透传 ， 1: 直驱 , 2: iBeacon

        sprintf((char*)sys_config.name, DEV_NAME_DEFAULT);  //设备名称

        sys_config.role = BLE_ROLE_PERIPHERAL;         //主从模式, 默认从机
        //sys_config.role = BLE_ROLE_CENTRAL;

        sprintf((char*)sys_config.pass, "000000");      //密码
        sys_config.type = 0;                            //鉴权模式
        osal_memset(sys_config.mac_addr, 0, sizeof(sys_config.mac_addr));

        sys_config.ever_connect_peripheral_mac_addr_conut = 0;
        sys_config.ever_connect_peripheral_mac_addr_index = 0;        
        //曾经成功连接过的从机地址
        osal_memset(sys_config.ever_connect_mac_status, 0, MAX_PERIPHERAL_MAC_ADDR*MAC_ADDR_CHAR_LEN);

        sys_config.try_connect_time_ms = 0;       // 尝试连接时间---目前无效 

        sys_config.rssi = 0;                      //  RSSI 信号值

        sys_config.rxGain = HCI_EXT_RX_GAIN_STD;   //  接收增益强度
        sys_config.txPower = 0;                    //  发射信号强度

        sys_config.ibeacon_adver_time_ms = 500;
        //  模块工作类型  0: 立即工作， 1: 等待AT+CON 或 AT+CONNL 命令
        sys_config.workMode = 0;          
    }
    else if(flag == PARA_PARI_FACTORY)
    {
        //sprintf((char*)sys_config.pass, "000000");      //密码
        osal_memset(sys_config.mac_addr, 0, sizeof(sys_config.mac_addr));
        sys_config.ever_connect_peripheral_mac_addr_conut = 0;
        sys_config.ever_connect_peripheral_mac_addr_index = 0;        
        osal_memset(sys_config.ever_connect_mac_status, 0, MAX_PERIPHERAL_MAC_ADDR*MAC_ADDR_CHAR_LEN);
    }

    GAPBondMgr_SetParameter( GAPBOND_ERASE_ALLBONDS, 0, NULL ); //清除绑定信息

    simpleBLE_WriteAllDataToFlash();
}

// 打印所有存储到nv flash的数据， 方便调试代码
void PrintAllPara(void)
{
    char strTemp[64];
    
    sprintf(strTemp, "sys_config.baudrate = %d\r\n", sys_config.baudrate);
    HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
    simpleBLE_Delay_1ms(100);
    
    sprintf(strTemp, "sys_config.parity = %d\r\n", sys_config.parity); 
    HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
    simpleBLE_Delay_1ms(100);

    sprintf(strTemp, "sys_config.stopbit = %d\r\n", sys_config.stopbit);
    HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
    simpleBLE_Delay_1ms(100);

    sprintf(strTemp, "sys_config.mode = %d\r\n", sys_config.mode);
    HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
    simpleBLE_Delay_1ms(100);

    sprintf(strTemp, "sys_config.name = %s\r\n", sys_config.name);
    HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
    simpleBLE_Delay_1ms(100);
    
    sprintf(strTemp, "sys_config.role = %d\r\n", sys_config.role);
    HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
    simpleBLE_Delay_1ms(100);
    
    sprintf(strTemp, "sys_config.pass = %s\r\n", sys_config.pass);
    HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
    simpleBLE_Delay_1ms(100);
    
    sprintf(strTemp, "sys_config.type = %d\r\n", sys_config.type);
    HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
    simpleBLE_Delay_1ms(100);

    sprintf(strTemp, "sys_config.mac_addr = %s\r\n", sys_config.mac_addr);
    HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
    simpleBLE_Delay_1ms(100);

    //曾经成功连接过的从机地址
    LCD_WRITE_STRING_VALUE( "addr_conut:", sys_config.ever_connect_peripheral_mac_addr_conut, 10, HAL_LCD_LINE_2 );

    for(int i = 0; i < sys_config.ever_connect_peripheral_mac_addr_conut; i++)
    {
        uint8 temp_addr[MAC_ADDR_CHAR_LEN+1] = {0};
        osal_memcpy(temp_addr, sys_config.ever_connect_mac_status[i], MAC_ADDR_CHAR_LEN);
        sprintf(strTemp, "[%d] = %s\r\n", i, temp_addr);
        HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
        simpleBLE_Delay_1ms(200);
    }
    
    sprintf(strTemp, "sys_config.try_connect_time_ms = %d\r\n", sys_config.try_connect_time_ms);
    HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
    simpleBLE_Delay_1ms(100);

    sprintf(strTemp, "sys_config.rssi = %d\r\n", sys_config.rssi);
    HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
    simpleBLE_Delay_1ms(100);

    sprintf(strTemp, "sys_config.txPower = %d\r\n", sys_config.txPower);
    HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
    simpleBLE_Delay_1ms(100);

    sprintf(strTemp, "sys_config.ibeacon_adver_time_ms = %d12\r\n", sys_config.ibeacon_adver_time_ms);
    HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
    simpleBLE_Delay_1ms(100);

    sprintf(strTemp, "sys_config.workMode = %d\r\n", sys_config.workMode);
    HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
    simpleBLE_Delay_1ms(100);
}

// 返回设备角色
//主从模式  0: 从机   1: 主机
BLE_ROLE GetBleRole()
{
    return sys_config.role;
}

// 判断蓝牙是否连接上
// 0: 未连接上
// 1: 已连接上
bool simpleBLE_IfConnected()
{
    if(GetBleRole() == BLE_ROLE_CENTRAL)//主机
    {          
        return ( simpleBLEState == BLE_STATE_CONNECTED  );
    }
    else
    {
        return (gapProfileState == GAPROLE_CONNECTED);
    }
}

// 增加从机地址， 注意， 需要连接成功后， 再增加该地址
void simpleBLE_SetPeripheralMacAddr(uint8 *pAddr)
{
    if(GetBleRole() == BLE_ROLE_CENTRAL)//主机
    {
       uint8 Addr[MAC_ADDR_CHAR_LEN];
       uint8 index;
       
       LCD_WRITE_STRING_VALUE( "_conut:", sys_config.ever_connect_peripheral_mac_addr_conut, 10, HAL_LCD_LINE_2 );
       // 要增加从机地址， 需要先检查我们的地址列表里是否已经存有改地址， 如果已经有了， 那么无需再增加
       for(index = 0; index < sys_config.ever_connect_peripheral_mac_addr_conut; index++)        
       {
          if(simpleBLE_GetPeripheralMacAddr(index, Addr))
          {
             if(osal_memcmp(Addr, pAddr, MAC_ADDR_CHAR_LEN))//地址一样时直接返回
             {
               LCD_WRITE_STRING_VALUE( "_index: return", index, 10, HAL_LCD_LINE_2 );
                //最新一次成功连接过的从机地址index， 用于针对AT+CONNL 这个指令
                sys_config.last_connect_peripheral_mac_addr_index = index;
                return;
             }
          }
          else
          {
             break;
          }
       }

       LCD_WRITE_STRING_VALUE( "_index:", sys_config.last_connect_peripheral_mac_addr_index, 10, HAL_LCD_LINE_2 );
       
       //只增加这多的从机地址， 超出后, 覆盖最先的一个地址记录，列表里只保存最近的     MAX_PERIPHERAL_MAC_ADDR    个地址
       osal_memcpy(sys_config.ever_connect_mac_status[sys_config.ever_connect_peripheral_mac_addr_index], pAddr, MAC_ADDR_CHAR_LEN);

       //最新一次成功连接过的从机地址index， 用于针对AT+CONNL 这个指令
       sys_config.last_connect_peripheral_mac_addr_index = sys_config.ever_connect_peripheral_mac_addr_index;

       sys_config.ever_connect_peripheral_mac_addr_index++;
       // 注意下面这个的技巧用法
       sys_config.ever_connect_peripheral_mac_addr_index %= MAX_PERIPHERAL_MAC_ADDR; 

       // 只记录  MAX_PERIPHERAL_MAC_ADDR 个最新的地址
       if(sys_config.ever_connect_peripheral_mac_addr_conut < MAX_PERIPHERAL_MAC_ADDR) 
       {
         sys_config.ever_connect_peripheral_mac_addr_conut++; 
       }

       LCD_WRITE_STRING_VALUE( "_conut2:", sys_config.ever_connect_peripheral_mac_addr_conut, 10, HAL_LCD_LINE_2 );

       // 保存地址 ， 以便重启机子后可以也使用 
       simpleBLE_WriteAllDataToFlash();
    }
}

// 读取从机地址, index < MAX_PERIPHERAL_MAC_ADDR
// 用于判断是否系统中已存有该Mac地址
/*
index: 应该是 < MAX_PERIPHERAL_MAC_ADDR,
*/
bool simpleBLE_GetPeripheralMacAddr(uint8 index, uint8 *pAddr)
{
    if(GetBleRole() == BLE_ROLE_CENTRAL)//主机
    {  
        if(index < sys_config.ever_connect_peripheral_mac_addr_conut
        /*&& index < MAX_PERIPHERAL_MAC_ADDR*/ )
        {
            osal_memcpy(pAddr, sys_config.ever_connect_mac_status[index], MAC_ADDR_CHAR_LEN);
            return TRUE;
        }
    }
    return FALSE;
}

//开机时判断到按键按下3秒， 恢复出厂设置
//按键定义为  p0.7
void CheckKeyForSetAllParaDefault(void) 
{
    uint8 i;
    uint32 old_time  = 30; 

    P0SEL &= ~0x80;     //设置P0.7为普通IO口  
    P0DIR &= ~0x80;     //按键接在P0.7口上，设P0.7为输入模式 
    P0INP &= ~0x80;     //打开P0.7上拉电阻

#if 0// for test only ， 用示波器测试时间
    while(1)
    {
        simpleBle_LedSetState(HAL_LED_MODE_ON);  
        simpleBLE_Delay_1ms(1);
        simpleBle_LedSetState(HAL_LED_MODE_OFF);  
        simpleBLE_Delay_1ms(1);
        simpleBle_LedSetState(HAL_LED_MODE_ON);  
        simpleBLE_Delay_1ms(100);
        simpleBle_LedSetState(HAL_LED_MODE_OFF);  
        simpleBLE_Delay_1ms(100);
    }
#endif

    //判断3s 內按键都没有松开
    while(--old_time)
    {
        if(P0_7 == 0)
        {
            simpleBle_LedSetState(HAL_LED_MODE_ON);  
            simpleBLE_Delay_1ms(100);
        }
        else
        {
            simpleBle_LedSetState(HAL_LED_MODE_OFF);  
            return;
        }        
    }

    //判断3s 內按键都没有松开， 那么就行恢复出厂设置，下面会先闪灯三下再恢复出厂设置然后重启
    if(old_time == 0)
    {
        simpleBLE_SetAllParaDefault(PARA_ALL_FACTORY);
        for(i = 0; i < 6; i++)    
        {
            simpleBle_LedSetState(HAL_LED_MODE_ON);  
            simpleBLE_Delay_1ms(100);
            simpleBle_LedSetState(HAL_LED_MODE_OFF);
            simpleBLE_Delay_1ms(100);
        }   
        // 重启， 实际上是利用看门狗了
        HAL_SYSTEM_RESET();     
    }
}

// 串行口 uart 初始化
void simpleBLE_NPI_init(void)
{
#if 1    
    SerialApp_InitEx(simpleBLE_NpiSerialCallback, sys_config.baudrate, 
        sys_config.parity, sys_config.stopbit );
#else
    NPI_InitTransport(simpleBLE_NpiSerialCallback);
#endif

    // 开机打印主机还是从机
    if(GetBleRole() == BLE_ROLE_CENTRAL)
    {
        //NPI_WriteTransport("Hello World Central\r\n",21);
        HalUARTWrite(SBP_UART_PORT, (uint8 *)"Hello World Central\r\n", 21);
    }
    else
    {
        //NPI_WriteTransport("Hello World Peripheral\r\n",24);
        HalUARTWrite(SBP_UART_PORT, (uint8 *)"Hello World Peripheral\r\n", 24);
    }
}

// 设置接收增益
void UpdateRxGain(void)
{
    // HCI_EXT_SetRxGainCmd()是用来设置发射功率的. 
    // rxGain - HCI_EXT_RX_GAIN_STD, HCI_EXT_RX_GAIN_HIGH
    HCI_EXT_SetRxGainCmd( HCI_EXT_RX_GAIN_STD );
}

// 设置发射功率
void UpdateTxPower(void)
{
        /*
#define LL_EXT_TX_POWER_MINUS_23_DBM                   0
#define LL_EXT_TX_POWER_MINUS_6_DBM                    1
#define LL_EXT_TX_POWER_0_DBM                          2
#define LL_EXT_TX_POWER_4_DBM                          3
        */
    // HCI_EXT_SetTxPowerCmd()是用来设置发射功率的. 有-23dbm, -6dbm, 0 dbm, +4dbm四个级别. 
    HCI_EXT_SetTxPowerCmd(3 - sys_config.txPower);
}

// 设置led灯的状态
void simpleBle_LedSetState(uint8 onoff)
{
  HalLedSet( HAL_LED_1, onoff);  //led常亮

  P0DIR |= 0x60;  // P0.6定义为输出
  P0_6 = onoff;  
}

// 保存RSSI 到系统变量
void simpleBle_SetRssi(int8 rssi)
{
    sys_config.rssi = rssi;
}

// 串口打印密码  -----测试用----
void simpleBle_PrintPassword()
{
    char strTemp[24] = {0};
    
    sprintf(strTemp, "Password:%s\r\n", sys_config.pass);
    HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
}

// 获取设备名称
uint8* simpleBle_GetAttDeviceName()
{
    return sys_config.name;
}

// 主机是否记录了从机地址
bool simpleBle_IFfHavePeripheralMacAddr( void )
{
    if(sys_config.ever_connect_peripheral_mac_addr_conut > 0)
        return TRUE;
    else
        return TRUE;
}

// 定时器任务定时执行函数， 用于设置led的状态----也可以增加一个定时器来做
void simpleBLE_performPeriodicTask( void )
{
    static uint8 count = 0;

    if(simpleBLE_CheckIfUse_iBeacon())  
    {
        static attHandleValueNoti_t pReport;
        pReport.len = 2;
        pReport.handle = 0x2E;
        osal_memcpy(pReport.value, "ab", 2);
        GATT_Notification( 0, &pReport, FALSE );

        simpleBle_LedSetState(HAL_LED_MODE_TOGGLE);           //取反
        return;
    }
    
    /*
    连线前，
        主机未记录从机地址时，每秒亮100ms；
        主机记录从机地址时，每秒亮900ms；
        从机每2秒亮1秒。
    连线后，
        主机与从机均为，LED每5秒亮100毫秒。
    */
    if(!simpleBLE_IfConnected())
    {    
        if(GetBleRole() == BLE_ROLE_CENTRAL)//主机
        {     
            if(simpleBle_IFfHavePeripheralMacAddr() == FALSE)//未记录地址
            {
                if(count == 0)
                {
                    simpleBle_LedSetState(HAL_LED_MODE_ON);  
                } 
                else if(count == 1)
                {
                    simpleBle_LedSetState(HAL_LED_MODE_OFF);
                }
            }
            else
            {
                if(count == 0)
                {
                    simpleBle_LedSetState(HAL_LED_MODE_ON);  
                } 
                else if(count == 9)
                {
                    simpleBle_LedSetState(HAL_LED_MODE_OFF);
                }
            }                    
            count++;
            count %= 10;     
        }  
        else//从机
        {
            if(count == 0)
            {
                simpleBle_LedSetState(HAL_LED_MODE_OFF);  
            } 
            else if(count == 10)
            {
                simpleBle_LedSetState(HAL_LED_MODE_ON);
            }

            count++;
            count %= 20;
        }
    }
    else// 连接后 主机与从机均为，LED每5秒亮100毫秒。(如果想省电， 可以不点灯)
    {
        if(count == 0)
        {
            simpleBle_LedSetState(HAL_LED_MODE_ON);  
        } 
        else if(count == 1)
        {
            simpleBle_LedSetState(HAL_LED_MODE_OFF);
        }
        count++;
        count %= 50; 

        // 发送自己的自定义数据， 实现自动数据串口透传
        simpleBLE_SendMyData_ForTest();
    }
}

// 获取鉴权要求, 0: 连接不需要密码,  1: 连接需要密码
bool simpleBle_GetIfNeedPassword()
{
    return sys_config.type;
}

// 获取连接密码
uint32 simpleBle_GetPassword()
{
    uint32 passcode;   
    
    passcode = str2Num(sys_config.pass, 6);
    passcode %= 1000000;

    return passcode;
}

// 判断是否是 iBeacon 广播模式
bool simpleBLE_CheckIfUse_iBeacon()
{
    return (sys_config.mode == BLE_MODE_iBeacon);
}

// 判断是否使能串口透传
bool simpleBLE_CheckIfUse_Uart2Uart()
{
    return (sys_config.mode == BLE_MODE_SERIAL);
}

// 判断是输入的形参-地址是否是需要去连接的地址，如果是， 返回真， 否则返回假
bool simpleBLE_GetToConnectFlag(uint8 *Addr)
{
    if(g_Central_connect_cmd  == BLE_CENTRAL_CONNECT_CMD_DISC)
    {
        return FALSE;
    }
    else if((g_Central_connect_cmd  == BLE_CENTRAL_CONNECT_CMD_CONNL)
    || (g_Central_connect_cmd  == BLE_CENTRAL_CONNECT_CMD_CONN))
    {
        if(sys_config.ever_connect_peripheral_mac_addr_conut > 0)
        {
            osal_memcpy(Addr, sys_config.ever_connect_mac_status[sys_config.last_connect_peripheral_mac_addr_index], MAC_ADDR_CHAR_LEN);            
            return TRUE; 
        }
    }
    else if(g_Central_connect_cmd  == BLE_CENTRAL_CONNECT_CMD_CON)
    {
        osal_memcpy(Addr, sys_config.connect_mac_addr, MAC_ADDR_CHAR_LEN);            
        return TRUE; 
    }

    return FALSE;
}

// 设置 iBeacon 的广播间隔
uint32 simpleBLE_GetiBeaconAdvertisingInterral()
{
    return sys_config.ibeacon_adver_time_ms;    
}

#if 1
// 串口回调函数， 下面把该回调函数里实现的功能讲解一下
/*
1, 思路:  当串口收到数据后，就会马上调用以下回调函数，在实际测试中发现，此回调
函数调用频繁， 如果你不执行NPI_ReadTransport函数进行读取， 那么这个回调函数就会
频繁地被执行，但是，你通过串口发送一段数据， 你本意是想处理这一完整一段的数据，所以，
我们在下面引入了时间的处理方法， 也即接收的数据够多或者超时，就读取一次数据， 
然后根据当前的状态决定执行，如果没有连接上，就把所有数据当做AT命令处理， 如果连接
上了，就把数据送到对端。
*/

//uart 回调函数
static void simpleBLE_NpiSerialCallback( uint8 port, uint8 events )
{
    (void)port;

    static uint32 old_time;     //老时间
    static uint32 old_time_data_len = 0;     //老时间是的数据长度    
    uint32 new_time;            //新时间
    bool ret;
    uint8 readMaxBytes = MD_UART_TRANS_LEN;
        
    if (events & (HAL_UART_RX_TIMEOUT | HAL_UART_RX_FULL))   //串口有数据
    {
        (void)port;
        uint8 numBytes = 0;

        //numBytes = NPI_RxBufLen();           //读出串口缓冲区有多少字节
        numBytes = Hal_UART_RxBufLen(SBP_UART_PORT);
        
        if(numBytes == 0)
        {
            LCD_WRITE_STRING_VALUE( "ERROR: numBytes=", numBytes, 10, HAL_LCD_LINE_1 );
            old_time_data_len = 0;
            return;
        }
        if(old_time_data_len == 0)
        {
            old_time = osal_GetSystemClock(); //有数据来时， 记录一下
            old_time_data_len = numBytes;
        }
        else
        {
            // 注意: 未连接上时， 有些AT 命令比较长， 所以需要开辟较大的缓冲区
            //       连接上以后， 收到每一能发送的数据不超过 MD_UART_TRANS_LEN 的字节数的限制
            //       因此，这里要限制一下
            if(!simpleBLE_IfConnected())
            {
               readMaxBytes = 22 ;    //这个值， 一般设置成 AT 命令中最长的字节数即可， (包含"\r\n" 计数)
            }
            else
            {
               readMaxBytes = MD_UART_TRANS_LEN;
            }
            
            new_time = osal_GetSystemClock(); //当前时间
            
            if( (numBytes >= readMaxBytes) 
                || ( (new_time - old_time) > 20/*ms*/))
            {
                uint8 sendBytes = 0;
                uint8 *buffer = osal_mem_alloc(readMaxBytes);

                if(!buffer)
                {
                    //NPI_WriteTransport("FAIL", 4);
                    HalUARTWrite(SBP_UART_PORT, (uint8 *)"FAIL", 4);
                    return;
                }
                
                // 
                if(numBytes > readMaxBytes)
                {
                    sendBytes = readMaxBytes;
                }
                else
                {
                    sendBytes = numBytes;
                }

                if(!simpleBLE_IfConnected())
                {
                    //numBytes = NpiReadBuffer(buf, sizeof(buf));
                    //NpiClearBuffer();
                    //NPI_ReadTransport(buffer,sendBytes);    //释放串口数据    
                    HalUARTRead(SBP_UART_PORT, buffer, sendBytes);
                    
                    if(sendBytes > 2 
                    && buffer[sendBytes-2] == '\r' 
                    && buffer[sendBytes-1] == '\n')
                    {//检测到 \r\n 结束的字符串， 表明是 AT 命令
                        ret = simpleBLE_AT_CMD_Handle(buffer, sendBytes);
                    }
                    else
                    {
                        ret = FALSE;
                    }
                    
                    if(ret == FALSE)
                    {
                        char strTemp[12];
                        //参数错误， 直接返回 "ERROR\r\n"， 不做任何参数更改
                        sprintf(strTemp, "ERROR\r\n");
                        HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
                    }
                }
                else
                {
                    if((GetBleRole() == BLE_ROLE_CENTRAL) && simpleBLECentralCanSend )             
                    {
                        if( simpleBLEChar6DoWrite )
                        {
                            char strTemp[24];

                            //NPI_ReadTransport(buffer,sendBytes);    //释放串口数据    
                            HalUARTRead (SBP_UART_PORT, buffer, sendBytes);

                            // 判断是否是查询 RSSI
                            if((sendBytes == 10) && str_cmp(buffer, "AT+RSSI?\r\n", 10))//AT+RSSI\r\n    
                            {
                                sprintf(strTemp, "OK+RSSI:%d\r\n", sys_config.rssi);
                                HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
                            }
                            else
                            {                        
                                //LCD_WRITE_STRING_VALUE( "sendBytes=", sendBytes, 10, HAL_LCD_LINE_1 );                            
                                simpleBLE_UartDataMain(buffer,sendBytes);
                            }
                        }
                        /*
                        else if(numBytes > 100)
                        {
                            NPI_ReadTransport(buffer,sendBytes);    //释放串口数据    
                        }
                        */
                    }
                    else if((GetBleRole() == BLE_ROLE_PERIPHERAL) && simpleBLEChar6DoWrite2)                    
                    {
                        //LCD_WRITE_STRING_VALUE( "sendBytes=", sendBytes, 10, HAL_LCD_LINE_1 );
                        //NPI_ReadTransport(buffer,sendBytes);    //释放串口数据    
                        HalUARTRead (SBP_UART_PORT, buffer, sendBytes);
                        simpleBLE_UartDataMain(buffer,sendBytes);
                    }                                       
                    else
                    {
                        //丢弃数据， 否则就会拖慢cpu
                        //NPI_ReadTransport(buffer,sendBytes);    //释放串口数据   
                        HalUARTRead (SBP_UART_PORT, buffer, sendBytes);
                    }
                }

                old_time = new_time;
                old_time_data_len = numBytes - sendBytes;


                osal_mem_free(buffer);
            }                
        }    
    }
}

// 处理: 串口发送过来的数据， 通过无线发送出去
void simpleBLE_UartDataMain(uint8 *buf, uint8 numBytes)
{
    if(FALSE == simpleBLE_CheckIfUse_Uart2Uart())     //未使用透传模式时不透传
        return;
    
    if(GetBleRole() == BLE_ROLE_CENTRAL )//主机
    {          
        if(simpleBLEChar6DoWrite 
        && ( simpleBLECharHd6 != 0)
        && simpleBLECentralCanSend)               //写入成功后再写入
        {
            attWriteReq_t AttReq;   

            LCD_WRITE_STRING_VALUE( "s=", numBytes, 10, HAL_LCD_LINE_1 );
            
            simpleBLEChar6DoWrite = FALSE;

            AttReq.handle = simpleBLECharHd6;
            AttReq.len = numBytes;
            AttReq.sig = 0;
            AttReq.cmd = 0;
            osal_memcpy(AttReq.value,buf,numBytes);
            GATT_WriteCharValue( 0, &AttReq, simpleBLETaskId );
        }
        else
        {
            LCD_WRITE_STRING_VALUE( "simpleBLEChar6DoWrite=", simpleBLEChar6DoWrite, 10, HAL_LCD_LINE_1 );
            LCD_WRITE_STRING_VALUE( "simpleBLECharHd6=", simpleBLECharHd6, 10, HAL_LCD_LINE_1 );
        }
    }
    else//从机
    {
        if(simpleBLEChar6DoWrite2)               //写入成功后再写入
        {      
#if 0 // 这种速度慢 SimpleProfile_SetParameter           
            simpleBLEChar6DoWrite2 = FALSE;
            SimpleProfile_SetParameter( SIMPLEPROFILE_CHAR6,numBytes, buf );
#else // 这种速度快 GATT_Notification
            static attHandleValueNoti_t pReport;
            pReport.len = numBytes;
            pReport.handle = 0x0035;
            osal_memcpy(pReport.value, buf, numBytes);
            GATT_Notification( 0, &pReport, FALSE );            
#endif            
        }            
        else
        {
            LCD_WRITE_STRING_VALUE( "simpleBLEChar6DoWrite=", simpleBLEChar6DoWrite2, 10, HAL_LCD_LINE_1 );
        }
    }
}
#endif

#if 1
// AT 命令处理 函数
bool simpleBLE_AT_CMD_Handle(uint8 *pBuffer, uint16 length)
{
    bool ret = TRUE;
    char strTemp[64];
    uint8 i;
    uint8 temp8;  
    bool restart = FALSE;
 
    //NPI_WriteTransport((uint8*)pBuffer, length); 
    // 1、测试
    if((length == 4) && str_cmp(pBuffer, "AT\r\n", 4))//AT    
    {
        sprintf(strTemp, "OK\r\n");
        HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
    }
    // 打印设置的参数  for test only
    else if((length == 8) && str_cmp(pBuffer, "AT+ALL\r\n", 8))//AT    
    {
        PrintAllPara();
    }
    // 2、查询、设置波特率
    else if((length == 10) && str_cmp(pBuffer, "AT+BAUD", 7))
    {
        /*
        发送：AT+BAUD2 
        返回：OK+Set:2 
        0---------9600 
        1---------19200 
        2---------38400 
        3---------57600 
        4---------115200
        */
        switch(pBuffer[7])
        {
        case '?':  //查询当前波特率
            sprintf(strTemp, "OK+Get:%d\r\n", sys_config.baudrate);
            HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
            break;
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':  //查询设置新的波特率
            sys_config.baudrate = pBuffer[7] - '0';
            simpleBLE_WriteAllDataToFlash();
            sprintf(strTemp, "OK+Set:%d\r\n", sys_config.baudrate);
            HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));

            restart = TRUE;  //直接重启即可
            break;
        default:
            ret = FALSE;            
            break;
        }        
    }
    // 3、设置串口校验
    else if((length == 10) && str_cmp(pBuffer, "AT+PARI", 7))
    {
        /*
        Para 范围 0,1,2 
        0: 无校验
        1: EVEN 
        2: ODD 
        Default: 0 
        */
        switch(pBuffer[7])
        {
        case '?':  
            sprintf(strTemp, "OK+Get:%d\r\n", sys_config.parity);
            HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
            break;
        case '0':
        case '1':
        case '2':
            sys_config.parity = pBuffer[7] - '0';        
            simpleBLE_WriteAllDataToFlash();
            sprintf(strTemp, "OK+Set:%d\r\n", sys_config.parity);
            HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));

            restart = TRUE;  //直接重启即可
            break;
        default:    
            ret = FALSE;            
            break;
        }        
    }    
    // 4、设置停止位
    else if((length == 10) && str_cmp(pBuffer, "AT+STOP", 7))
    {
        /*
        Para: 0~1 
        0: 1 停止位
        1: 2 停止位
        Default: 0
        */
        switch(pBuffer[7])
        {
        case '?':  
            sprintf(strTemp, "OK+Get:%d\r\n", sys_config.stopbit);
            HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
            break;
        case '0':
        case '1':
            sys_config.stopbit = pBuffer[7] - '0';  
            simpleBLE_WriteAllDataToFlash();
            sprintf(strTemp, "OK+Set:%d\r\n", sys_config.stopbit);
            HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));

            restart = TRUE;  //直接重启即可
            break;
        default:    
            ret = FALSE;            
            break;
        }        
    }       
    // 5. 设置模块工作模式
    else if((length == 10) && str_cmp(pBuffer, "AT+MODE", 7))
    {
        /*
        Para: 0 ~ 1
        0: 开启串口透传模式
        1: 关闭串口透传模式
        2: iBeacon 广播模式
        Default: 0 
        */
        switch(pBuffer[7])
        {
        case '?':  
            sprintf(strTemp, "OK+Get:%d\r\n", sys_config.mode);
            HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
            break;
        case '0':
        case '1':
        case '2':
            sys_config.mode = pBuffer[7] - '0';            
            simpleBLE_WriteAllDataToFlash();
            sprintf(strTemp, "OK+Set:%d\r\n", sys_config.mode);
            HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));

            restart = TRUE;  //直接重启即可
            break;
        default:    
            ret = FALSE;            
            break;
        }        
    }          
    // 6、查询、设置设备名称
    else if((length >=10 && length <= 20) && str_cmp(pBuffer, "AT+NAME", 7))
    {
        /*
        Para1：设备名称
        最长 11 位数字或字母，
        含中划线和下划线，不建
        议用其它字符。
        Default：DEV_NAME_DEFAULT(见宏定义)
        */
        //int nameLen = length - 7;
        
        switch(pBuffer[7])
        {
        case '?':  
            sprintf(strTemp, "OK+Get:%s\r\n", sys_config.name);
            HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
            break;
        default:
            osal_memset(sys_config.name, 0, sizeof(sys_config.name));
            osal_memcpy(sys_config.name, pBuffer + 7, length - 7);
            simpleBLE_WriteAllDataToFlash();
            sprintf(strTemp, "OK+Set:%s\r\n", sys_config.name);
            HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));

            restart = TRUE;  //直接重启即可
            break;
        }        
    }    
    //7. 恢复默认设置(Renew)
    else if((length == 10) && str_cmp(pBuffer, "AT+RENEW", 8))
    {
        sprintf(strTemp, "OK+RENEW\r\n");
        HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
        
        simpleBLE_SetAllParaDefault(PARA_ALL_FACTORY);

        restart = TRUE;  //直接重启即可
    }
    //8. 模块复位，重启(Reset)
    else if((length == 10) && str_cmp(pBuffer, "AT+RESET", 8))
    {
        restart = TRUE;  //直接重启即可
    }
    // 9、查询、设置主从模式
    else if((length == 10) && str_cmp(pBuffer, "AT+ROLE", 7))
    {
        /*
        Para1: 0 ~ 1 
        1: 主设备
        0: 从设备
        Default: 0 
        */
        switch(pBuffer[7])
        {
        case '?':  
            sprintf(strTemp, "OK+Get:%d\r\n", sys_config.role);
            HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
            break;
        case '0':
        case '1':
            temp8 = pBuffer[7] - '0';            
            if(temp8 == 0)
            {
              sys_config.role = BLE_ROLE_PERIPHERAL;
            }
            else
            {
              sys_config.role = BLE_ROLE_CENTRAL;
            }
            simpleBLE_WriteAllDataToFlash();
            sprintf(strTemp, "OK+Set:%d\r\n", sys_config.role);
            HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));

            restart = TRUE;  //直接重启即可
            break;
        default:    
            ret = FALSE;            
            break;
        }        
    }    
    // 10、 查询、设置配对密码
    else if(((length == 10) && str_cmp(pBuffer, "AT+PASS?", 8))
        || ((length == 15) && str_cmp(pBuffer, "AT+PASS", 7)))
    {
        /*
        Para1: 000000~999999 
        Default：000000
        */
        switch(pBuffer[7])
        {
        case '?':  
            sprintf(strTemp, "OK+PASS:%s\r\n", sys_config.pass);
            HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
            break;
        default:
            osal_memcpy(sys_config.pass, pBuffer + 7, 6);
            sys_config.pass[6] = 0;
            simpleBLE_WriteAllDataToFlash();
            sprintf(strTemp, "OK+Set:%s\r\n", sys_config.pass);
            HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
            break;
        }
    }   
    // 11、 设置模块鉴权工作类型
    else if((length == 10) && str_cmp(pBuffer, "AT+TYPE", 7))
    {
        /*
        Para: 0 ~ 1 
        0: 连接不需要密码
        1: 连接需要密码
        Default: 0 
        */
        switch(pBuffer[7])
        {
        case '?':  
            sprintf(strTemp, "OK+Get:%d\r\n", sys_config.type);
            HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
            break;
        case '0':
        case '1':
            sys_config.type = pBuffer[7] - '0';            
            simpleBLE_WriteAllDataToFlash();
            sprintf(strTemp, "OK+Set:%d\r\n", sys_config.type);
            HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));

            restart = TRUE;  //直接重启即可
            break;
        default:    
            ret = FALSE;            
            break;
        }        
    }       
    // 12、 查询本机 MAC 地址
    else if((length >= 10) && str_cmp(pBuffer, "AT+ADDR?", 8))
    {        
        sprintf(strTemp, "OK+LADD:%s\r\n", sys_config.mac_addr);
        HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
    }    
    // 13、 连接最后一次连接成功的从设备
    else if((length == 10) && str_cmp(pBuffer, "AT+CONNL", 8))
    {
        /*
        Para: L, N, E,F
        L:连接中、N:空地址
        E:连接错误、F:连接失败
        */
        if((GetBleRole() == BLE_ROLE_CENTRAL))
        {        
            uint8 para[4] = {'L','N','E','F'};
            int8 id = 0;

            g_Central_connect_cmd  = BLE_CENTRAL_CONNECT_CMD_CONNL;

            if(sys_config.ever_connect_peripheral_mac_addr_conut == 0)
            {
                id = 1;
            }
            else
            {
                id = 0;
                
                  // 开始扫描
                {
                    extern void simpleBLEStartScan();
                    simpleBLEStartScan();
                }
            }

            sprintf(strTemp, "AT+CONN%c\r\n", para[id]);
            HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
        }
        else
        {
            ret = FALSE;
        }
    }
    // 14、连接指定蓝牙地址的主设备或从设备
    else if((length == 20) && str_cmp(pBuffer, "AT+CON", 6))
    {
        /*
        Para1: MAC地址、
        如: 0017EA0923AE
        Para2: A, E, F
        A: 连接中
        E: 连接错误
        F: 连接失败
        */
        uint8 para[3] = {'A','E','F'};
        uint8 id = 0;
        
        if((GetBleRole() == BLE_ROLE_CENTRAL))
        {
            g_Central_connect_cmd  = BLE_CENTRAL_CONNECT_CMD_CON;
            osal_memcpy(sys_config.connect_mac_addr, pBuffer+6, MAC_ADDR_CHAR_LEN);

            sprintf(strTemp, "AT+CONN%c\r\n", para[id]);
            HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));

            // 开始扫描
            {
                extern void simpleBLEStartScan();
                simpleBLEStartScan();
            }
        }
        else
        {
            ret = FALSE;
        }
    }
    // 15、 清除主设备配对信息
    else if((length == 10) && str_cmp(pBuffer, "AT+CLEAR", 8))
    {
        if((GetBleRole() == BLE_ROLE_CENTRAL))
        {        
            simpleBLE_SetAllParaDefault(PARA_PARI_FACTORY);
            //PrintAllPara();

            // 下面这个事 系统 API 函数
            GAPBondMgr_SetParameter( GAPBOND_ERASE_ALLBONDS, 0, NULL );
            
            sprintf(strTemp, "OK+CLEAR\r\n");
            HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
        }
        else
        {
            ret = FALSE;
        }
    }
    // 16、查询成功连接过的从机地址 
    // 备注：此指令只有在主设备时才有效；从设备时不接受此指令
    else if((length == 10) && str_cmp(pBuffer, "AT+RADD?", 8) 
        && (BLE_ROLE_CENTRAL == GetBleRole()))//仅主机有效
    {
        if((GetBleRole() == BLE_ROLE_CENTRAL))
        {        
            if(sys_config.ever_connect_peripheral_mac_addr_conut == 0)//无地址
            {
                sprintf(strTemp, "OK+RADD:NULL\r\n");
                HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
            }
            else
            {        
                for(i = 0; i<sys_config.ever_connect_peripheral_mac_addr_conut; i++)
                {
                    sprintf(strTemp, "OK+RADD:%s\r\n", sys_config.ever_connect_mac_status[i]);
                    HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
                }
            }
        }
        else
        {
            ret = FALSE;
        }
    }
    // 17、 查询软件版本
    else if((length == 10) && str_cmp(pBuffer, "AT+VERS?", 8))
    {
        extern const uint8 devInfoFirmwareRev[];
        sprintf(strTemp, "%s\r\n", devInfoFirmwareRev);
        HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
    }
    // 18、 设置主模式下尝试连接时间
    else if((length == 10) && str_cmp(pBuffer, "AT+TCON", 7))
    {
        /*
        指令	                应答	            参数
        查询：AT+TCON?	        OK+TCON:[para] 	
        设置：AT+TCON[para]	    OK+Set:[para] 	    Para: 000000～009999 
                                                    000000 代表持续连接，其
                                                    余代表尝试的毫秒数
                                                    Default:001000
        */
        if((GetBleRole() == BLE_ROLE_CENTRAL))
        {        
            if(pBuffer[7] == '?')
            {
                sprintf(strTemp, "%06d\r\n", sys_config.try_connect_time_ms);
                HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
            }
            else
            {             
                sys_config.try_connect_time_ms = 10000;//_atoi(pBuffer+7);
                simpleBLE_WriteAllDataToFlash();
                sprintf(strTemp, "OK+Set:%06d\r\n", sys_config.try_connect_time_ms);
                HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
            }
        }
        else
        {
            ret = FALSE;
        }        
    }    
    // 19、 读取 RSSI 信号值
    else if((length == 10) && str_cmp(pBuffer, "AT+RSSI?", 10))
    {
        sprintf(strTemp, "OK+RSSI:%d\r\n", sys_config.rssi);
        HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
    }
    // 20、  改变模块发射信号强度
    else if((length == 10) && str_cmp(pBuffer, "AT+TXPW", 7))
    {
        /*
        指令	        应答	            参数
        查询：          AT+TXPW?	        OK+ TXPW:[para]	
        设置：          AT+TXPW[para]	    OK+Set:[para]	Para: 0 ~ 3
                                            0: 4dbm、1: 0dbm
                                            2: -6dbm、3: -23dbm
                                            Default: 0
        */
        if(pBuffer[7] == '?')
        {
            sprintf(strTemp, "AT+TXPW:%d\r\n", sys_config.txPower);
            HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
        }
        else
        {
            sys_config.txPower = pBuffer[7] - '0';
            if(sys_config.txPower > 3)
            {
                sys_config.txPower = 0;
            }
            simpleBLE_WriteAllDataToFlash();
            sprintf(strTemp, "OK+Set:%d\r\n", sys_config.txPower);
            HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));

#if 1
        /*
#define LL_EXT_TX_POWER_MINUS_23_DBM                   0
#define LL_EXT_TX_POWER_MINUS_6_DBM                    1
#define LL_EXT_TX_POWER_0_DBM                          2
#define LL_EXT_TX_POWER_4_DBM                          3
        */
            // HCI_EXT_SetTxPowerCmd()是用来设置发射功率的. 有-23dbm, -6dbm, 0 dbm, +4dbm四个级别. 
            HCI_EXT_SetTxPowerCmd(3 - sys_config.txPower);
#endif      

            restart = TRUE;  //直接重启即可
         }
    }        
    // 21、  改变模块作为ibeacon基站广播时间间隔
    else if((length == 10 || length == 15) && str_cmp(pBuffer, "AT+TIBE", 7))
    {
        /*
        指令	        应答	        参数
        查询：          AT+TIBE?	    OK+ TIBE:[para]	
        设置：          AT+TIBE[para]	OK+Set:[para]	Para: 000000～009999 
                                        000000 代表持续广播，其
                                        余代表尝试的毫秒数
                                        Default:000500
        */
        if((GetBleRole() == BLE_ROLE_PERIPHERAL))
        {        
            if(pBuffer[7] == '?')
            {
                sprintf(strTemp, "AT+TIBE:%06d\r\n", sys_config.ibeacon_adver_time_ms);
                HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
            }
            else
            {   
                sys_config.ibeacon_adver_time_ms = str2Num(pBuffer+7, 6);
                if(sys_config.ibeacon_adver_time_ms <= 9999)
                {
                    simpleBLE_WriteAllDataToFlash();
                    sprintf(strTemp, "OK+Set:%06d\r\n", sys_config.ibeacon_adver_time_ms);
                    HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));

                    restart = TRUE;  //直接重启即可
                }
            }
        }
        else
        {
            ret = FALSE;
        }        
    }
    // 22、  设置模块工作类型 
    else if((length == 10) && str_cmp(pBuffer, "AT+IMME", 7))
    {
        /*
        指令	        应答	        参数
        查询：          AT+IMME?	    OK+Get:[para]	Para: 0~1
        设置：          AT+IMME[para]	OK+Set:[para]	Para: 0~1
                                        000000 代表持续广播，其
                                        0: 立即工作， 
                                        1: 等待AT+CON 或 AT+CONNL 命令
                                        Default:0
        */
        if(pBuffer[7] == '?')
        {
            sprintf(strTemp, "OK+Get:%d\r\n", sys_config.workMode);
            HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
        }
        else
        {   
            sys_config.workMode = str2Num(pBuffer+7, 1);
            if(sys_config.workMode <= 1)
            {
                simpleBLE_WriteAllDataToFlash();
                sprintf(strTemp, "OK+Set:%d\r\n", sys_config.workMode);
                HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
            }

            restart = TRUE;  //直接重启即可
        }
    }    
    // 23、 设置开始主模式下的从机扫描
    else if((length == 10) && str_cmp(pBuffer, "AT+DISC?\r\n", 10))
    {
        /*
        AT+DISC? OK+DISCS 无
        该指令为主模式、手动工作模式指令。使用前须满足 AT+ROLE1，AT+IMME1
        条件。
        发送 AT+DISC?指令后，模块若处于待机状态，返回 OK+DISCS 后开始搜索。
        如果搜到 BLE 设备会返回 OK+DISC:123456789012,其中数字字符串代表对
        方蓝牙地址，最多返回 6 个，下标从 0 开始。搜索完成后返回 OK+DISCE。
        如只搜到二个设备的例子：
        发送: AT+DISC?
        接收: OK+DISCS
        接收: OK+DISC:123456789012
        接收: OK+DISCE
        如果要连接搜到的设备，下标从 0 开始。
        发送: AT+CONN0 连接搜到的第一个设备
        发送: AT+CONN1 连接搜到的第二个设备
        依此类推
        */
        if((GetBleRole() == BLE_ROLE_CENTRAL))
        {
            g_Central_connect_cmd  = BLE_CENTRAL_CONNECT_CMD_DISC;

            sprintf(strTemp, "OK+DISCS\r\n");
            HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));

            // 开始扫描
            {
                extern void simpleBLEStartScan();
                simpleBLEStartScan();
            }
        }
        else
        {
            ret = FALSE;
        }
    }
    // 24、 连接已经发现的从机， 输入的是下标号
    else if((length == 10) && str_cmp(pBuffer, "AT+CONN", 7))
    {
        /* 
        发送: AT+CONN0 连接搜到的第一个设备
        发送: AT+CONN1 连接搜到的第二个设备
        依此类推
        */
        if((GetBleRole() == BLE_ROLE_CENTRAL))
        {
            //sprintf(strTemp, "OK+DISCS\r\n");
            //NPI_WriteTransport((uint8*)strTemp, osal_strlen(strTemp)); 

            g_Central_connect_cmd  = BLE_CENTRAL_CONNECT_CMD_CONN;

            temp8 = pBuffer[7] - '0';
            // 开始扫描
            {
                extern bool simpleBLEConnect(uint8 index);
                simpleBLEConnect(temp8);
            }
        }
        else
        {
            ret = FALSE;
        }        
    }
    else
    {
        ret = FALSE;
    }

    if(restart)//如果该标志已设置，稍微延时后重启
    {
        simpleBLE_Delay_1ms(200);      //设置参数后，适当延时， 以便上一次发送的数据正常发送出去
        HAL_SYSTEM_RESET(); 
    }
    
    return ret;
}
#endif

#if 1
/*
很多朋友问我们， 如何实现把主机或从机上的传感器数据直接发送到对端并通过主机的串口
透传出去， 下面我们就能实现这个功能， 不过到底需要什么样的传感器， 以及什么样的数据
就需要你自己来组织了， 下面这个函数每100ms执行一次:
都可以把数据发送到对端， 对端通过串口透传出去。
下面给出一个样例: 实现把字符串发送到对方
*/
void simpleBLE_SendMyData_ForTest()
{
#if 0  
    static uint8 count_100ms = 0;
    uint8 numBytes;

    // 这里存在一些问题
    
    count_100ms++;
    if(count_100ms == 1)//本函数每100ms被执行一次， 计数10次就是1s
    {
        char strTemp[24] = {0};

        if((GetBleRole() == BLE_ROLE_CENTRAL) && simpleBLEChar6DoWrite && simpleBLECentralCanSend)               
        {
            sprintf(strTemp, "[%8ldms]Amo1\r\n", osal_GetSystemClock());
            //把你的数据组织到 strTemp， 就ok了, 注意不要超过 SIMPLEPROFILE_CHAR6_LEN 的大小
            //如果你发送的数据要超过 SIMPLEPROFILE_CHAR6_LEN， 那么最好的办法， 就是启动一个定时器，然后每定时器到， 就发送一段数据
            // 定时器的启动， 请参考       osal_start_timerEx( simpleBLETaskId, SBP_PERIODIC_EVT, SBP_PERIODIC_EVT_PERIOD );
            numBytes = (osal_strlen(strTemp) > SIMPLEPROFILE_CHAR6_LEN) ? SIMPLEPROFILE_CHAR6_LEN : osal_strlen(strTemp);            
            simpleBLE_UartDataMain((uint8*)strTemp, numBytes);
        }
        else if((GetBleRole() == BLE_ROLE_PERIPHERAL) && simpleBLEChar6DoWrite2)                    
        {
            sprintf(strTemp, "[%8ldms]Amo2\r\n", osal_GetSystemClock());
            //把你的数据组织到 strTemp， 就ok了, 注意不要超过 SIMPLEPROFILE_CHAR6_LEN 的大小
            //如果你发送的数据要超过 SIMPLEPROFILE_CHAR6_LEN， 那么最好的办法， 就是启动一个定时器，然后每定时器到， 就发送一段数据
            // 定时器的启动， 请参考       osal_start_timerEx( simpleBLETaskId, SBP_PERIODIC_EVT, SBP_PERIODIC_EVT_PERIOD );
            numBytes = (osal_strlen(strTemp) > SIMPLEPROFILE_CHAR6_LEN) ? SIMPLEPROFILE_CHAR6_LEN : osal_strlen(strTemp);
            simpleBLE_UartDataMain((uint8*)strTemp, numBytes);
        }

        count_100ms = 0;
    }
#endif    
}
#endif

