/******************************************************************************

 @file  simpleBLECentral.c

 @brief This file contains the Simple BLE Central sample application for use
        with the CC2540 Bluetooth Low Energy Protocol Stack.

 Group: WCS, BTS
 Target Device: CC2540, CC2541

 ******************************************************************************

 Copyright (c) 2010-2016, Texas Instruments Incorporated
 All rights reserved.

 IMPORTANT: Your use of this Software is limited to those specific rights
 granted under the terms of a software license agreement between the user
 who downloaded the software, his/her employer (which must be your employer)
 and Texas Instruments Incorporated (the "License"). You may not use this
 Software unless you agree to abide by the terms of the License. The License
 limits your use, and you acknowledge, that the Software may not be modified,
 copied or distributed unless embedded on a Texas Instruments microcontroller
 or used solely and exclusively in conjunction with a Texas Instruments radio
 frequency transceiver, which is integrated into your product. Other than for
 the foregoing purpose, you may not use, reproduce, copy, prepare derivative
 works of, modify, distribute, perform, display or sell this Software and/or
 its documentation for any purpose.

 YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
 PROVIDED 揂S IS?WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
 NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
 TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
 NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
 LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
 INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
 OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
 OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
 (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

 Should you have any questions regarding your right to use this Software,
 contact Texas Instruments Incorporated at www.TI.com.

 ******************************************************************************
 Release Name: ble_sdk_1.4.2.2
 Release Date: 2016-06-09 06:57:10
 *****************************************************************************/

/*********************************************************************
 * INCLUDES
 */
#include <stdio.h>
#include <string.h>
#include "bcomdef.h"
#include "OSAL.h"
#include "OSAL_PwrMgr.h"
#include "OnBoard.h"
#include "hal_led.h"
#include "hal_key.h"
#include "hal_lcd.h"
#include "hal_uart.h"
#include "gatt.h"
#include "ll.h"
#include "hci.h"
#include "gapgattserver.h"
#include "gattservapp.h"
#include "central.h"
#include "gapbondmgr.h"
#include "simpleGATTprofile.h"
//#include "md_uart_profile.h"
#include "simpleBLECentral.h"
//#include "npi.h"
#include "simpleble.h"

/*********************************************************************
 * MACROS
 */

// Length of bd addr as a string
#define B_ADDR_STR_LEN                        15

/*********************************************************************
 * CONSTANTS
 */

// Maximum number of scan responses
#define DEFAULT_MAX_SCAN_RES                  6//8

// Scan duration in ms
#define DEFAULT_SCAN_DURATION                 1500//4000  默认扫描时间 ms

// Discovey mode (limited, general, all)
#define DEFAULT_DISCOVERY_MODE                DEVDISC_MODE_ALL

// TRUE to use active scan
#define DEFAULT_DISCOVERY_ACTIVE_SCAN         TRUE

// TRUE to use white list during discovery
#define DEFAULT_DISCOVERY_WHITE_LIST          FALSE

// TRUE to use high scan duty cycle when creating link
#define DEFAULT_LINK_HIGH_DUTY_CYCLE          FALSE

// TRUE to use white list when creating link
#define DEFAULT_LINK_WHITE_LIST               FALSE

// Default RSSI polling period in ms
#define DEFAULT_RSSI_PERIOD                   1000

// Whether to enable automatic parameter update request when a connection is formed
#define DEFAULT_ENABLE_UPDATE_REQUEST         FALSE

// Minimum connection interval (units of 1.25ms) if automatic parameter update request is enabled
#define DEFAULT_UPDATE_MIN_CONN_INTERVAL      16//400

// Maximum connection interval (units of 1.25ms) if automatic parameter update request is enabled
#define DEFAULT_UPDATE_MAX_CONN_INTERVAL      32//800

// Slave latency to use if automatic parameter update request is enabled
#define DEFAULT_UPDATE_SLAVE_LATENCY          0

// Supervision timeout value (units of 10ms) if automatic parameter update request is enabled
#define DEFAULT_UPDATE_CONN_TIMEOUT           599

// Default passcode
#define DEFAULT_PASSCODE                      19655

// Default GAP pairing mode
#define DEFAULT_PAIRING_MODE                  GAPBOND_PAIRING_MODE_WAIT_FOR_REQ

// Default MITM mode (TRUE to require passcode or OOB when pairing)
#define DEFAULT_MITM_MODE                     FALSE

// Default bonding mode, TRUE to bond
#define DEFAULT_BONDING_MODE                  TRUE

// Default GAP bonding I/O capabilities
#define DEFAULT_IO_CAPABILITIES               GAPBOND_IO_CAP_DISPLAY_ONLY

// Default service discovery timer delay in ms
#define DEFAULT_SVC_DISCOVERY_DELAY           2000

// TRUE to filter discovery results on desired service UUID
#define DEFAULT_DEV_DISC_BY_SVC_UUID          TRUE

// Application states

// Discovery states
enum
{
    BLE_DISC_STATE_IDLE,                // Idle
    BLE_DISC_STATE_SVC,                 // Service discovery
    BLE_DISC_STATE_CHAR                 // Characteristic discovery
};

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */

/*********************************************************************
 * EXTERNAL VARIABLES
 */

/*********************************************************************
 * EXTERNAL FUNCTIONS
 */

/*********************************************************************
 * LOCAL VARIABLES
 */

// Task ID for internal task/event processing
uint8 simpleBLETaskId;

// GAP GATT Attributes
//static const uint8 simpleBLEDeviceName[GAP_DEVICE_NAME_LEN] = "Simple BLE Central";

// Number of scan results and scan result index
static uint8 simpleBLEScanRes;
static uint8 simpleBLEScanIdx;

static uint8 g_addr[MAC_ADDR_CHAR_LEN];
// Scan result list
static gapDevRec_t simpleBLEDevList[DEFAULT_MAX_SCAN_RES];

// Scanning state
static uint8 simpleBLEScanning = FALSE;

// RSSI polling state
static uint8 simpleBLERssi = FALSE;
static bool gReadingRssi = FALSE;

// Connection handle of current connection
uint16 simpleBLEConnHandle = GAP_CONNHANDLE_INIT;

// Application state
uint8 simpleBLEState = BLE_STATE_IDLE;

// Discovery state
static uint8 simpleBLEDiscState = BLE_DISC_STATE_IDLE;

// Discovered service start and end handle
static uint16 simpleBLESvcStartHdl = 0;
static uint16 simpleBLESvcEndHdl = 0;

// Discovered characteristic handle
uint16 simpleBLECharHdl = 0;
uint16 simpleBLECharHd6 = 0;
bool simpleBLEChar6DoWrite = TRUE;
bool simpleBLECentralCanSend = FALSE;          //  主机可发送数据

// Value to write
//static uint8 simpleBLECharVal = 0;

// Value read/write toggle
//static bool simpleBLEDoWrite = FALSE;

// GATT read/write procedure state
//static bool simpleBLEProcedureInProgress = FALSE;

/*********************************************************************
 * LOCAL FUNCTIONS
 */
static void simpleBLECentralProcessGATTMsg( gattMsgEvent_t *pMsg );
static void simpleBLECentralRssiCB( uint16 connHandle, int8  rssi );
static uint8 simpleBLECentralEventCB( gapCentralRoleEvent_t *pEvent );
static void simpleBLECentralPasscodeCB( uint8 *deviceAddr, uint16 connectionHandle,
                                        uint8 uiInputs, uint8 uiOutputs );
static void simpleBLECentralPairStateCB( uint16 connHandle, uint8 state, uint8 status );
static void simpleBLECentral_HandleKeys( uint8 shift, uint8 keys );
static void simpleBLECentral_ProcessOSALMsg( osal_event_hdr_t *pMsg );
static void simpleBLEGATTDiscoveryEvent( gattMsgEvent_t *pMsg );
static void simpleBLECentralStartDiscovery( void );
static bool simpleBLEFindSvcUuid( uint16 uuid, uint8 *pData, uint8 dataLen );
static void simpleBLEAddDeviceInfo( uint8 *pAddr, uint8 addrType );
static char *bdAddr2Str ( uint8 *pAddr );
static void SBC_EnableNotification( uint16 connHandle, uint16 attrHandle );
static void handle_central_led_state(void);
static void handle_rssi(void);
/*********************************************************************
 * PROFILE CALLBACKS
 */

// GAP Role Callbacks
static const gapCentralRoleCB_t simpleBLERoleCB =
{
    simpleBLECentralRssiCB,       // RSSI callback
    simpleBLECentralEventCB       // Event callback
};

// Bond Manager Callbacks
static const gapBondCBs_t simpleBLEBondCB =
{
    simpleBLECentralPasscodeCB,
    simpleBLECentralPairStateCB
};

/*********************************************************************
 * PUBLIC FUNCTIONS
 */

/*********************************************************************
 * @fn      SimpleBLECentral_Init
 *
 * @brief   Initialization function for the Simple BLE Central App Task.
 *          This is called during initialization and should contain
 *          any application specific initialization (ie. hardware
 *          initialization/setup, table initialization, power up
 *          notification).
 *
 * @param   task_id - the ID assigned by OSAL.  This ID should be
 *                    used to send messages and set timers.
 *
 * @return  none
 */
void SimpleBLECentral_Init( uint8 task_id )
{
    simpleBLETaskId = task_id;

    // Setup Central Profile
    {
        uint8 scanRes = DEFAULT_MAX_SCAN_RES;
        GAPCentralRole_SetParameter ( GAPCENTRALROLE_MAX_SCAN_RES, sizeof( uint8 ), &scanRes );
    }

    // Setup GAP
    GAP_SetParamValue( TGAP_GEN_DISC_SCAN, DEFAULT_SCAN_DURATION );
    GAP_SetParamValue( TGAP_LIM_DISC_SCAN, DEFAULT_SCAN_DURATION );
    //GGS_SetParameter( GGS_DEVICE_NAME_ATT, GAP_DEVICE_NAME_LEN, (uint8 *) simpleBLEDeviceName );
    GGS_SetParameter( GGS_DEVICE_NAME_ATT, GAP_DEVICE_NAME_LEN, simpleBle_GetAttDeviceName() );


    // Setup the GAP Bond Manager
    {
        uint32 passkey = simpleBle_GetPassword();
        uint8 pairMode;// = DEFAULT_PAIRING_MODE;
        uint8 mitm = DEFAULT_MITM_MODE;
        uint8 ioCap = DEFAULT_IO_CAPABILITIES;
        //uint8 bonding = DEFAULT_BONDING_MODE;

        /*
        bonding就是把配对信息记录下来, 下次就不用配对了. 不bonding下次就还得配对.
        所以我们从机这里把 bonding = FALSE 的后果就是， 主设备每次连接都必须输入密码
        而把  bonding = TRUE 后， 主设备只需第一次连接时输入密码， 后面断开后都不需要再次输入密码即可连接
        ---------------amomcu.com-------------------------
        */
        uint8 bonding = FALSE;
        if(simpleBle_GetIfNeedPassword())
        {
            pairMode = GAPBOND_PAIRING_MODE_INITIATE;   //配对模式，置配成等待主机的配对请求
        }
        else
        {
            pairMode = GAPBOND_PAIRING_MODE_WAIT_FOR_REQ;
        }

        GAPBondMgr_SetParameter( GAPBOND_DEFAULT_PASSCODE, sizeof( uint32 ), &passkey );
        GAPBondMgr_SetParameter( GAPBOND_PAIRING_MODE, sizeof( uint8 ), &pairMode );
        GAPBondMgr_SetParameter( GAPBOND_MITM_PROTECTION, sizeof( uint8 ), &mitm );
        GAPBondMgr_SetParameter( GAPBOND_IO_CAPABILITIES, sizeof( uint8 ), &ioCap );
        GAPBondMgr_SetParameter( GAPBOND_BONDING_ENABLED, sizeof( uint8 ), &bonding );
    }

    // Initialize GATT Client
    VOID GATT_InitClient();

    // Register to receive incoming ATT Indications/Notifications
    GATT_RegisterForInd( simpleBLETaskId );

    // Initialize GATT attributes
    GGS_AddService( GATT_ALL_SERVICES );         // GAP
    GATTServApp_AddService( GATT_ALL_SERVICES ); // GATT attributes

    // Register for all key events - This app will handle all key events
    //RegisterForKeys( simpleBLETaskId );

    // makes sure LEDs are off
    HalLedSet( (HAL_LED_1 | HAL_LED_2), HAL_LED_MODE_OFF );

    // Setup a delayed profile startup
    osal_set_event( simpleBLETaskId, START_DEVICE_EVT );
}

void simpleBLEStartScan(void)
{
    LCD_WRITE_STRING( "--simpleBLEStartScan", HAL_LCD_LINE_2 );

    simpleBLECentralCanSend = FALSE;

    //开机后就马上进行，自动开始搜索
    if ( !simpleBLEScanning/* & simpleBLEScanRes == 0 */)
    {
        simpleBLEScanning = TRUE;
        simpleBLEScanRes = 0;
        GAPCentralRole_StartDiscovery( DEVDISC_MODE_ALL,
                                       TRUE,
                                       DEFAULT_DISCOVERY_WHITE_LIST );
        LCD_WRITE_STRING( "Start Scanning...", HAL_LCD_LINE_1 );
        SerialPrintString("Start Scanning...");
    }
    else
    {
        LCD_WRITE_STRING( "In Scanning...", HAL_LCD_LINE_1 );
        SerialPrintString("In Scanning...");
    }
}

/*********************************************************************
 * @fn      SimpleBLECentral_ProcessEvent
 *
 * @brief   Simple BLE Central Application Task event processor.  This function
 *          is called to process all events for the task.  Events
 *          include timers, messages and any other user defined events.
 *
 * @param   task_id  - The OSAL assigned task ID.
 * @param   events - events to process.  This is a bit map and can
 *                   contain more than one event.
 *
 * @return  events not processed
 */
uint16 SimpleBLECentral_ProcessEvent( uint8 task_id, uint16 events )
{

    VOID task_id; // OSAL required parameter that isn't used in this function

    if ( events & SYS_EVENT_MSG )
    {
        uint8 *pMsg;

        if ( (pMsg = osal_msg_receive( simpleBLETaskId )) != NULL )
        {
            simpleBLECentral_ProcessOSALMsg( (osal_event_hdr_t *)pMsg );

            // Release the OSAL message
            osal_msg_deallocate( pMsg );
        }

        // return unprocessed events
        return (events ^ SYS_EVENT_MSG);
    }

    if ( events & START_DEVICE_EVT )
    {
        // Start the Device
        VOID GAPCentralRole_StartDevice( (gapCentralRoleCB_t *) &simpleBLERoleCB );

        // Register with bond manager after starting device
        GAPBondMgr_Register( (gapBondCBs_t *) &simpleBLEBondCB );

        //simpleBLEStartScan();
#if 1
        osal_start_timerEx( simpleBLETaskId, SBP_CENTRAL_LED_EVT, 101 );
#else
        osal_start_timerEx( simpleBLETaskId, SBP_CENTRAL_PERIODIC_EVT, 100 );
#endif
        CheckKeyForSetAllParaDefault(); //按键按下3秒， 恢复出厂设置

        osal_start_timerEx( simpleBLETaskId, SBP_CENTRAL_START_SCAN_EVT, 2016 );
        return ( events ^ START_DEVICE_EVT );
    }
#if 1
    if ( events & SBP_CENTRAL_PERIODIC_EVT )
    {
        // Restart timer
        if ( SBP_PERIODIC_EVT_PERIOD )
        {
            osal_start_timerEx( simpleBLETaskId, SBP_CENTRAL_PERIODIC_EVT, SBP_PERIODIC_EVT_PERIOD );
        }

        // Perform periodic application task
        simpleBLE_performPeriodicTask();


        if ( simpleBLEState == BLE_STATE_CONNECTED )
        {
            if ( !simpleBLERssi )
            {
                simpleBLERssi = TRUE;
                GAPCentralRole_StartRssi( simpleBLEConnHandle, DEFAULT_RSSI_PERIOD );
                //LCD_WRITE_STRING( "RSSI Start...", HAL_LCD_LINE_1 );
            }
        }
        else
        {
            if(simpleBLERssi)
            {
                GAPCentralRole_CancelRssi( simpleBLEConnHandle );
                //LCD_WRITE_STRING( "RSSI Cancelled", HAL_LCD_LINE_1 );
            }
        }

        return (events ^ SBP_CENTRAL_PERIODIC_EVT);
    }
    #endif
    if ( events & SBP_CENTRAL_RSSI_EVT )
    {
        //handle_rssi();
        return (events ^ SBP_CENTRAL_RSSI_EVT);
    }
    if ( events & SBP_CENTRAL_LED_EVT )
    {
        handle_central_led_state();
        return (events ^ SBP_CENTRAL_LED_EVT);
    }
    if ( events & SBP_CENTRAL_DISCOVERY_EVT )
    {
        simpleBLECentralStartDiscovery( );
        return ( events ^ SBP_CENTRAL_DISCOVERY_EVT );
    }
    if ( events & SBP_CENTRAL_SAVE_EVT )
    {
        if (g_Central_connect_cmd  == BLE_CENTRAL_CONNECT_CMD_CONN)
        {
            simpleBLE_SetPeripheralMacAddr((uint8 *)bdAddr2Str(simpleBLEDevList[simpleBLEScanIdx].addr) + 2);
        }
        else
        {
            simpleBLE_SetPeripheralMacAddr(g_addr);
        }
        return ( events ^ SBP_CENTRAL_SAVE_EVT );
    }
    if ( events & SBP_CENTRAL_START_SCAN_EVT )
    {
        // 判断如果设置过 AT+IMME1 的，则去连接最近连接过的从机
        if(1 == sys_config.workMode)
        {
            g_Central_connect_cmd  = BLE_CENTRAL_CONNECT_CMD_CONNL;
            // 开始扫描
            simpleBLEStartScan();
        }
        return ( events ^ SBP_CENTRAL_START_SCAN_EVT );
    }
    if ( events & SBP_CENTRAL_EN_NOTIFY_EVT )
    {
        SBC_EnableNotification(simpleBLEConnHandle, simpleBLECharHd6 + 1);
        simpleBLECentralCanSend = TRUE;
        HalUARTWrite(SBP_UART_PORT, (uint8 *)"Connected\r\n", 11);
        return ( events ^ SBP_CENTRAL_EN_NOTIFY_EVT );
    }
    if ( events & SBP_CENTRAL_READ_RSSI_EVT )
    {
        char strTemp[24];
        GAPCentralRole_CancelRssi( simpleBLEConnHandle );
        sprintf(strTemp, "OK+RSSI:%d\r\n", sys_config.rssi);
        HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, strlen(strTemp));
        gReadingRssi = FALSE;
        return ( events ^ SBP_CENTRAL_READ_RSSI_EVT );
    }
    
    // Discard unknown events
    return 0;
}

/*
    未连接，
        主机未记录从机地址时，每秒亮100ms；
        主机记录从机地址时，每秒亮900ms；
        从机每2秒亮1秒。
    已连接，
        主机与从机均为，LED每5秒亮100毫秒。
    */
static void handle_central_led_state(void)
{//这是从机哟
    static uint8 led_state = 0;
    if (simpleBLE_IfConnected())//如果已连接
    {
        if (led_state == 0)
        {
            led_state = 1;
            simpleBle_LedSetState(HAL_LED_MODE_ON);
            osal_start_timerEx( simpleBLETaskId, SBP_CENTRAL_LED_EVT, 100 );
        }
        else
        {
            led_state = 0;
            simpleBle_LedSetState(HAL_LED_MODE_OFF);
            osal_start_timerEx( simpleBLETaskId, SBP_CENTRAL_LED_EVT, 5000 );
        }
    }
    else//如果未连接
    {
        if(simpleBle_IFfHavePeripheralMacAddr() == FALSE)//未记录地址，每秒亮100ms
        {
            if (led_state == 0)
            {
                led_state = 1;
                simpleBle_LedSetState(HAL_LED_MODE_ON);
                osal_start_timerEx( simpleBLETaskId, SBP_CENTRAL_LED_EVT, 100 );
            }
            else
            {
                led_state = 0;
                simpleBle_LedSetState(HAL_LED_MODE_OFF);
                osal_start_timerEx( simpleBLETaskId, SBP_CENTRAL_LED_EVT, 900 );
            }
        }
        else//每秒亮900ms
        {
            if (led_state == 0)
            {
                led_state = 1;
                simpleBle_LedSetState(HAL_LED_MODE_ON);
                osal_start_timerEx( simpleBLETaskId, SBP_CENTRAL_LED_EVT, 800 );
            }
            else
            {
                led_state = 0;
                simpleBle_LedSetState(HAL_LED_MODE_OFF);
                osal_start_timerEx( simpleBLETaskId, SBP_CENTRAL_LED_EVT, 200 );
            }
        }
    }
}

static void handle_rssi(void)
{
    if ( simpleBLEState == BLE_STATE_CONNECTED )
    {
        if ( !simpleBLERssi )
        {
            simpleBLERssi = TRUE;
            GAPCentralRole_StartRssi( simpleBLEConnHandle, DEFAULT_RSSI_PERIOD );
            //LCD_WRITE_STRING( "RSSI Start...", HAL_LCD_LINE_1 );
        }
    }
    else
    {
        if(simpleBLERssi)
        {
            GAPCentralRole_CancelRssi( simpleBLEConnHandle );
            //LCD_WRITE_STRING( "RSSI Cancelled", HAL_LCD_LINE_1 );
        }
    }
}

void SBC_read_rssi(void)
{
    #if 1
    if (gReadingRssi == FALSE)
    {
        gReadingRssi = TRUE;
        GAPCentralRole_StartRssi( simpleBLEConnHandle, 100 );
        osal_start_timerEx( simpleBLETaskId, SBP_CENTRAL_READ_RSSI_EVT, 145 );
    }
    #else
    SBC_EnableNotification(simpleBLEConnHandle, simpleBLECharHd6 + 1);
    #endif
}

/*********************************************************************
 * @fn      simpleBLECentral_ProcessOSALMsg
 *
 * @brief   Process an incoming task message.
 *
 * @param   pMsg - message to process
 *
 * @return  none
 */
static void simpleBLECentral_ProcessOSALMsg( osal_event_hdr_t *pMsg )
{
    switch ( pMsg->event )
    {
    case KEY_CHANGE:
        simpleBLECentral_HandleKeys( ((keyChange_t *)pMsg)->state, ((keyChange_t *)pMsg)->keys );
        break;

    case GATT_MSG_EVENT:
        simpleBLECentralProcessGATTMsg( (gattMsgEvent_t *) pMsg );
        break;
    }
}

/*********************************************************************
 * @fn      simpleBLECentral_HandleKeys
 *
 * @brief   Handles all key events for this device.
 *
 * @param   shift - true if in shift/alt.
 * @param   keys - bit field for key events. Valid entries:
 *                 HAL_KEY_SW_2
 *                 HAL_KEY_SW_1
 *
 * @return  none
 */
uint8 gStatus;
static void simpleBLECentral_HandleKeys( uint8 shift, uint8 keys )
{
    (void)shift;  // Intentionally unreferenced parameter
#if 0
    if ( keys & HAL_KEY_UP )
    {
        // Start or stop discovery
        if ( simpleBLEState != BLE_STATE_CONNECTED )
        {
            if ( !simpleBLEScanning )
            {
                simpleBLEScanning = TRUE;
                simpleBLEScanRes = 0;

                LCD_WRITE_STRING( "Discovering...", HAL_LCD_LINE_1 );
                LCD_WRITE_STRING( "", HAL_LCD_LINE_2 );

                GAPCentralRole_StartDiscovery( DEFAULT_DISCOVERY_MODE,
                                               DEFAULT_DISCOVERY_ACTIVE_SCAN,
                                               DEFAULT_DISCOVERY_WHITE_LIST );
            }
            else
            {
                GAPCentralRole_CancelDiscovery();
            }
        }
        else if ( simpleBLEState == BLE_STATE_CONNECTED &&
                  simpleBLECharHdl != 0 &&
                  simpleBLEProcedureInProgress == FALSE )
        {
            uint8 status;

            // Do a read or write as long as no other read or write is in progress
            if ( simpleBLEDoWrite )
            {
                // Do a write
                attWriteReq_t req;

                req.pValue = GATT_bm_alloc( simpleBLEConnHandle, ATT_WRITE_REQ, 1, NULL );
                if ( req.pValue != NULL )
                {
                    req.handle = simpleBLECharHdl;
                    req.len = 1;
                    req.pValue[0] = simpleBLECharVal;
                    req.sig = 0;
                    req.cmd = 0;
                    status = GATT_WriteCharValue( simpleBLEConnHandle, &req, simpleBLETaskId );
                    if ( status != SUCCESS )
                    {
                        GATT_bm_free( (gattMsg_t *)&req, ATT_WRITE_REQ );
                    }
                }
                else
                {
                    status = bleMemAllocError;
                }
            }
            else
            {
                // Do a read
                attReadReq_t req;

                req.handle = simpleBLECharHdl;
                status = GATT_ReadCharValue( simpleBLEConnHandle, &req, simpleBLETaskId );
            }

            if ( status == SUCCESS )
            {
                simpleBLEProcedureInProgress = TRUE;
                simpleBLEDoWrite = !simpleBLEDoWrite;
            }
        }
    }

    if ( keys & HAL_KEY_LEFT )
    {
        // Display discovery results
        if ( !simpleBLEScanning && simpleBLEScanRes > 0 )
        {
            // Increment index of current result (with wraparound)
            simpleBLEScanIdx++;
            if ( simpleBLEScanIdx >= simpleBLEScanRes )
            {
                simpleBLEScanIdx = 0;
            }

            LCD_WRITE_STRING_VALUE( "Device", simpleBLEScanIdx + 1,
                                    10, HAL_LCD_LINE_1 );
            LCD_WRITE_STRING( bdAddr2Str( simpleBLEDevList[simpleBLEScanIdx].addr ),
                              HAL_LCD_LINE_2 );
        }
    }

    if ( keys & HAL_KEY_RIGHT )
    {
        // Connection update
        if ( simpleBLEState == BLE_STATE_CONNECTED )
        {
            GAPCentralRole_UpdateLink( simpleBLEConnHandle,
                                       DEFAULT_UPDATE_MIN_CONN_INTERVAL,
                                       DEFAULT_UPDATE_MAX_CONN_INTERVAL,
                                       DEFAULT_UPDATE_SLAVE_LATENCY,
                                       DEFAULT_UPDATE_CONN_TIMEOUT );
        }
    }

    if ( keys & HAL_KEY_CENTER )
    {
        uint8 addrType;
        uint8 *peerAddr;

        // Connect or disconnect
        if ( simpleBLEState == BLE_STATE_IDLE )
        {
            // if there is a scan result
            if ( simpleBLEScanRes > 0 )
            {
                // connect to current device in scan result
                peerAddr = simpleBLEDevList[simpleBLEScanIdx].addr;
                addrType = simpleBLEDevList[simpleBLEScanIdx].addrType;

                simpleBLEState = BLE_STATE_CONNECTING;

                GAPCentralRole_EstablishLink( DEFAULT_LINK_HIGH_DUTY_CYCLE,
                                              DEFAULT_LINK_WHITE_LIST,
                                              addrType, peerAddr );

                LCD_WRITE_STRING( "Connecting", HAL_LCD_LINE_1 );
                LCD_WRITE_STRING( bdAddr2Str( peerAddr ), HAL_LCD_LINE_2 );
            }
        }
        else if ( simpleBLEState == BLE_STATE_CONNECTING ||
                  simpleBLEState == BLE_STATE_CONNECTED )
        {
            // disconnect
            simpleBLEState = BLE_STATE_DISCONNECTING;

            gStatus = GAPCentralRole_TerminateLink( simpleBLEConnHandle );

            LCD_WRITE_STRING( "Disconnecting", HAL_LCD_LINE_1 );
        }
    }

    if ( keys & HAL_KEY_DOWN )
    {
        // Start or cancel RSSI polling
        if ( simpleBLEState == BLE_STATE_CONNECTED )
        {
            if ( !simpleBLERssi )
            {
                simpleBLERssi = TRUE;
                GAPCentralRole_StartRssi( simpleBLEConnHandle, DEFAULT_RSSI_PERIOD );
            }
            else
            {
                simpleBLERssi = FALSE;
                GAPCentralRole_CancelRssi( simpleBLEConnHandle );

                LCD_WRITE_STRING( "RSSI Cancelled", HAL_LCD_LINE_1 );
            }
        }
    }
#endif
}

/*********************************************************************
 * @fn      simpleBLECentralProcessGATTMsg
 *
 * @brief   Process GATT messages
 *
 * @return  none
 */
static void simpleBLECentralProcessGATTMsg( gattMsgEvent_t *pMsg )
{
    if ( simpleBLEState != BLE_STATE_CONNECTED )
    {
        // In case a GATT message came after a connection has dropped,
        // ignore the message
        return;
    }

    if ( ( pMsg->method == ATT_READ_RSP ) ||
            ( ( pMsg->method == ATT_ERROR_RSP ) &&
              ( pMsg->msg.errorRsp.reqOpcode == ATT_READ_REQ ) ) )
    {
        if ( pMsg->method == ATT_ERROR_RSP )
        {
#if (defined HAL_LCD) && (HAL_LCD == TRUE)
            uint8 status = pMsg->msg.errorRsp.errCode;

            LCD_WRITE_STRING_VALUE( "Read Error", status, 10, HAL_LCD_LINE_1 );
#endif
        }
        else
        {
            // After a successful read, display the read value
#if (defined HAL_LCD) && (HAL_LCD == TRUE)
            uint8 valueRead = pMsg->msg.readRsp.pValue[0];

            LCD_WRITE_STRING_VALUE( "Read rsp:", valueRead, 10, HAL_LCD_LINE_1 );
#endif
        }

        //simpleBLEProcedureInProgress = FALSE;
    }
    else if ( ( pMsg->method == ATT_WRITE_RSP ) ||
              ( ( pMsg->method == ATT_ERROR_RSP ) &&
                ( pMsg->msg.errorRsp.reqOpcode == ATT_WRITE_REQ ) ) )
    {

        if ( pMsg->method == ATT_ERROR_RSP )
        {
#if defined(DEBUG_SERIAL)
            uint8 status = pMsg->msg.errorRsp.errCode;

            LCD_WRITE_STRING_VALUE( "Write Error", status, 10, HAL_LCD_LINE_1 );
#endif
            //SerialPrintString("Write Error: ");
            SerialPrintValue("Write Error: ", status, 10);
            simpleBLEChar6DoWrite = TRUE;
        }
        else
        {
            // After a succesful write, display the value that was written and increment value
            //LCD_WRITE_STRING_VALUE( "Write sent:", simpleBLECharVal++, 10, HAL_LCD_LINE_1 );
            // 这个变量用于表明上一次写数据到从机已经成功， 可用于判断写数据时的判断， 以确保数据的完整性
            simpleBLEChar6DoWrite = TRUE;
            SerialPrintString("Write OK\r\n");
        }

        //simpleBLEProcedureInProgress = FALSE;

    }
    else if ( simpleBLEDiscState != BLE_DISC_STATE_IDLE )
    {
        simpleBLEGATTDiscoveryEvent( pMsg );
    }
    else if ( ( pMsg->method == ATT_HANDLE_VALUE_NOTI ) )   //通知
    {
        if( pMsg->msg.handleValueNoti.handle == simpleBLECharHd6/*0x0035*/)   //CHAR6的通知  串口打印
        {
            //SerialPrintString("wow.\r\n");
            if(simpleBLE_CheckIfUse_Uart2Uart())     //使用透传模式时才透传
            {
                //NPI_WriteTransport(pMsg->msg.handleValueNoti.value, pMsg->msg.handleValueNoti.len );
                HalUARTWrite(SBP_UART_PORT, pMsg->msg.handleValueNoti.pValue, pMsg->msg.handleValueNoti.len);
            }
        }
    }
    GATT_bm_free( &pMsg->msg, pMsg->method );
}

/*********************************************************************
 * @fn      simpleBLECentralRssiCB
 *
 * @brief   RSSI callback.
 *
 * @param   connHandle - connection handle
 * @param   rssi - RSSI
 *
 * @return  none
 */
static void simpleBLECentralRssiCB( uint16 connHandle, int8 rssi )
{
    //LCD_WRITE_STRING_VALUE( "RSSI -dB:", (uint8) (-rssi), 10, HAL_LCD_LINE_1 );
#if defined(DEBUG_SERIAL)
    char strTemp[64];
    sprintf(strTemp, "sys_config.rssi = %d\r\n", rssi);
    SerialPrintString(strTemp);
#endif
    simpleBle_SetRssi(rssi);
}

/*********************************************************************
 * @fn      simpleBLECentralEventCB
 *
 * @brief   Central event callback function.
 *
 * @param   pEvent - pointer to event structure
 *
 * @return  TRUE if safe to deallocate event message, FALSE otherwise.
 */
static uint8 simpleBLECentralEventCB( gapCentralRoleEvent_t *pEvent )
{
    switch ( pEvent->gap.opcode )
    {
    case GAP_DEVICE_INIT_DONE_EVENT:
    {
        LCD_WRITE_STRING( "BLE Central", HAL_LCD_LINE_1 );
        LCD_WRITE_STRING( bdAddr2Str( pEvent->initDone.devAddr ),  HAL_LCD_LINE_2 );
        SerialPrintString("GAP_DEVICE_INIT_DONE_EVENT\r\n");
    }
    break;

    case GAP_DEVICE_INFO_EVENT:
    {
        SerialPrintString("GAP_DEVICE_INFO_EVENT\r\n");
        // if filtering device discovery results based on service UUID
        if ( DEFAULT_DEV_DISC_BY_SVC_UUID == TRUE )
        {
            if ( simpleBLEFindSvcUuid( SIMPLEPROFILE_SERV_UUID,
                                       pEvent->deviceInfo.pEvtData,
                                       pEvent->deviceInfo.dataLen ) )
            {
    #if defined(DEBUG_SERIAL)
                char strTemp[64];
                sprintf(strTemp, "rssi = %d, mac = %s\r\n", pEvent->deviceInfo.rssi, bdAddr2Str(pEvent->deviceInfo.addr));
                SerialPrintString(strTemp);
    #endif
                simpleBLEAddDeviceInfo( pEvent->deviceInfo.addr, pEvent->deviceInfo.addrType );
            }
        }
    }
    break;

    case GAP_DEVICE_DISCOVERY_EVENT:
    {
        bool ifDoConnect = FALSE;

        // discovery complete
        simpleBLEScanning = FALSE;
        SerialPrintString("GAP_DEVICE_DISCOVERY_EVENT\r\n");
        // if not filtering device discovery results based on service UUID
        if ( DEFAULT_DEV_DISC_BY_SVC_UUID == FALSE )
        {
            // Copy results
            simpleBLEScanRes = pEvent->discCmpl.numDevs;
            osal_memcpy( simpleBLEDevList, pEvent->discCmpl.pDevList,
                         (sizeof( gapDevRec_t ) * pEvent->discCmpl.numDevs) );
        }

        LCD_WRITE_STRING_VALUE( "Devices Found", simpleBLEScanRes,
                                10, HAL_LCD_LINE_1 );
        if ( simpleBLEScanRes > 0 )
        {
            LCD_WRITE_STRING( "<- To Select", HAL_LCD_LINE_2 );
            LCD_WRITE_STRING_VALUE( "simpleBLEState = ", simpleBLEState, 10, HAL_LCD_LINE_2 );
            //这里可以决定连接到所搜索到的哪一个从机， 这里可以进行从机地址码的判断， 比如， 可以连接到最近一次连接成功的mac地址的从机
            //HalLedSet(HAL_LED_3, HAL_LED_MODE_ON );   //开LED3
            if ( simpleBLEState == BLE_STATE_IDLE )
            {
                uint8 addrType;
                uint8 *peerAddr;
                uint8 i;
                uint8 Addr[MAC_ADDR_CHAR_LEN+1] = {0};
                //bool ret = FALSE;;

                for(i = 0; i < simpleBLEScanRes; i++)
                {
                    LCD_WRITE_STRING_VALUE( "--->peerAddr ", i, 10, HAL_LCD_LINE_2 );
                    LCD_WRITE_STRING( bdAddr2Str( simpleBLEDevList[i].addr ), HAL_LCD_LINE_2 );
                }
                LCD_WRITE_STRING( "---------------", HAL_LCD_LINE_2 );
#if 0
                ret = simpleBLE_GetPeripheralMacAddr(0, Addr);
                LCD_WRITE_STRING_VALUE( "--->ret ", ret, 10, HAL_LCD_LINE_2 );

                simpleBLEScanIdx = 0;
                for(i = 0; i < simpleBLEScanRes; i++)
                {
                    LCD_WRITE_STRING_VALUE( "peerAddr ", i, 10, HAL_LCD_LINE_2 );
                    LCD_WRITE_STRING( bdAddr2Str( simpleBLEDevList[i].addr ), HAL_LCD_LINE_2 );

                    // connect to current device in scan result
                    peerAddr = simpleBLEDevList[i].addr;
                    addrType = simpleBLEDevList[i].addrType;

                    if(ret == FALSE) //如果没有记录地址， 则记录地址， 并连接第一个从设备
                    {
                        simpleBLE_SetPeripheralMacAddr((uint8 *)bdAddr2Str( simpleBLEDevList[i].addr ) + 2);
                        simpleBLE_WriteAllDataToFlash();
                        simpleBLEScanIdx = i;
                        ifDoConnect = TRUE;
                        break;
                    }
                    else
                    {
                        //指定连接某设备, 如果该次扫描没有之前连接过的地址， 则不建立连接
                        if(osal_memcmp(Addr, bdAddr2Str( simpleBLEDevList[i].addr ) + 2, MAC_ADDR_CHAR_LEN))
                        {
                            simpleBLEScanIdx = i;
                            ifDoConnect = TRUE;
                            break;
                        }
                    }
                }
#else
                LCD_WRITE_STRING_VALUE( "g_Central_connect_cmd = ", g_Central_connect_cmd, 10, HAL_LCD_LINE_2 );
                SerialPrintValue("g_Central_connect_cmd=", g_Central_connect_cmd, 10);
                // 打印从机地址， 格式:  OK+DISC:123456789012
                if(g_Central_connect_cmd  == BLE_CENTRAL_CONNECT_CMD_DISC)
                {
                    char strTemp[24] = {0};
                    for(i = 0; i < simpleBLEScanRes; i++)
                    {
                        sprintf(strTemp, "OK+DISC:");
                        osal_memcpy(strTemp + 8,  bdAddr2Str( simpleBLEDevList[i].addr ) + 2, MAC_ADDR_CHAR_LEN);
                        strTemp[20] = '\r';
                        strTemp[21] = '\n';
                        strTemp[22] = 0;
                        HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
                    }

                    // 搜索完成后返回 OK+DISCE。
                    sprintf(strTemp, "OK+DISCE\r\n");
                    HalUARTWrite(SBP_UART_PORT, (uint8 *)strTemp, osal_strlen(strTemp));
                }
                else if(g_Central_connect_cmd  == BLE_CENTRAL_CONNECT_CMD_CONNL)
                {
                    if(simpleBLE_GetToConnectFlag(Addr))
                    {
                        SerialPrintString("ConAddr ");
                        SerialPrintString(Addr);
                        SerialPrintString("\r\n");
                        // 连接指定的mac 地址的从机设备
                        //simpleBLEScanIdx = 0;
                        for(i = 0; i < simpleBLEScanRes; i++)
                        {
                            LCD_WRITE_STRING_VALUE( "peerAddr ", i, 10, HAL_LCD_LINE_2 );
                            LCD_WRITE_STRING( bdAddr2Str( simpleBLEDevList[i].addr ), HAL_LCD_LINE_2 );
                            SerialPrintString("peerAddr ");
                            SerialPrintString(bdAddr2Str( simpleBLEDevList[i].addr ));
                            SerialPrintString("\r\n");
                            // connect to current device in scan result
                            peerAddr = simpleBLEDevList[i].addr;
                            addrType = simpleBLEDevList[i].addrType;

                            if(osal_memcmp(Addr, bdAddr2Str( simpleBLEDevList[i].addr ) + 2, MAC_ADDR_CHAR_LEN))
                            {
                                //simpleBLEScanIdx = i;
                                ifDoConnect = TRUE;
                                memcpy(g_addr, Addr, MAC_ADDR_CHAR_LEN);
                                break;
                            }
                        }
                    }
                }
                else if(g_Central_connect_cmd  == BLE_CENTRAL_CONNECT_CMD_CONN)
                {
                    //连接指定最后成功的从机
                    // 连接从机
                    if(simpleBLE_GetToConnectFlag(Addr))
                    {
                        uint8 tempstr[13] = {0};
                        osal_memcpy(tempstr, Addr, 12);
                        LCD_WRITE_STRING_VALUE( "ConAddr ", 0, 10, HAL_LCD_LINE_2 );
                        LCD_WRITE_STRING( (char *)tempstr, HAL_LCD_LINE_2 );
                        SerialPrintString("ConAddr ");
                        SerialPrintString(tempstr);
                        SerialPrintString("\r\n");

                        // 连接指定的mac 地址的从机设备
                        //simpleBLEScanIdx = 0;
                        for(i = 0; i < simpleBLEScanRes; i++)
                        {
                            LCD_WRITE_STRING_VALUE( "peerAddr ", i, 10, HAL_LCD_LINE_2 );
                            LCD_WRITE_STRING( bdAddr2Str( simpleBLEDevList[i].addr ), HAL_LCD_LINE_2 );
                            SerialPrintString("peerAddr ");
                            SerialPrintString(bdAddr2Str( simpleBLEDevList[i].addr ));
                            SerialPrintString("\r\n");
                            // connect to current device in scan result
                            peerAddr = simpleBLEDevList[i].addr;
                            addrType = simpleBLEDevList[i].addrType;

                            if(osal_memcmp(Addr, bdAddr2Str( simpleBLEDevList[i].addr ) + 2, MAC_ADDR_CHAR_LEN))
                            {
                                //simpleBLEScanIdx = i;
                                ifDoConnect = TRUE;
                                break;
                            }
                        }
                    }
                }
                else if(g_Central_connect_cmd  == BLE_CENTRAL_CONNECT_CMD_CON)
                {
                    //连接指定地址的从机
                    if (simpleBLE_GetToConnectFlag(Addr))
                    {
                        uint8 tempstr[13] = {0};
                        osal_memcpy(tempstr, Addr, 12);
                        LCD_WRITE_STRING_VALUE( "ConAddr ", 0, 10, HAL_LCD_LINE_2 );
                        LCD_WRITE_STRING( (char *)tempstr, HAL_LCD_LINE_2 );

                        SerialPrintValue("simpleBLEScanRes = ", simpleBLEScanRes, 10);
                        // 连接指定的mac 地址的从机设备
                        //simpleBLEScanIdx = 0;
                        for(i = 0; i < simpleBLEScanRes; i++)
                        {
                            LCD_WRITE_STRING_VALUE( "peerAddr ", i, 10, HAL_LCD_LINE_2 );
                            LCD_WRITE_STRING( bdAddr2Str( simpleBLEDevList[i].addr ), HAL_LCD_LINE_2 );
                            SerialPrintString("peerAddr = ");
                            SerialPrintString((uint8*)bdAddr2Str( simpleBLEDevList[i].addr ));
                            SerialPrintString("\r\n");

                            // connect to current device in scan result
                            peerAddr = simpleBLEDevList[i].addr;
                            addrType = simpleBLEDevList[i].addrType;

                            if(osal_memcmp(Addr, bdAddr2Str( simpleBLEDevList[i].addr ) + 2, MAC_ADDR_CHAR_LEN))
                            {
                                //simpleBLEScanIdx = i;
                                ifDoConnect = TRUE;
                                memcpy(g_addr, Addr, MAC_ADDR_CHAR_LEN);
                                break;
                            }
                        }
                    }
                }
#endif
                if(ifDoConnect)
                {
                    simpleBLEState = BLE_STATE_CONNECTING;

                    //NPI_WriteTransport("Connecting\r\n", 12);
                    HalUARTWrite(SBP_UART_PORT, (uint8 *)"Connecting\r\n", 12);

                    LCD_WRITE_STRING( "Connecting", HAL_LCD_LINE_1 );
                    LCD_WRITE_STRING( bdAddr2Str( pEvent->linkCmpl.devAddr ), HAL_LCD_LINE_2 );

                    GAPCentralRole_EstablishLink( DEFAULT_LINK_HIGH_DUTY_CYCLE,
                                                  DEFAULT_LINK_WHITE_LIST,
                                                  addrType, peerAddr );
                }
                else
                {
                    LCD_WRITE_STRING( "Continue scanning", HAL_LCD_LINE_1 );

                    // 继续扫描
                    simpleBLEScanning  = FALSE;
                    //simpleBLEScanRes = 0;
                    //simpleBLEStartScan();
                }
            }

            //HalLedSet(HAL_LED_3, HAL_LED_MODE_OFF );

            // initialize scan index to last device
            //simpleBLEScanIdx = simpleBLEScanRes;
        }

        if( ifDoConnect == FALSE )
        {
            osal_start_timerEx( simpleBLETaskId, SBP_CENTRAL_START_SCAN_EVT, 3016 );
        }
    }
    break;

    case GAP_LINK_ESTABLISHED_EVENT:
    {
        if ( pEvent->gap.hdr.status == SUCCESS )
        {
            //SerialPrintString("GAP_LINK_ESTABLISHED_EVENT\r\n");
            SerialPrintValue("GAP_LINK_ESTABLISHED_EVENT", simpleBLECharHdl, 10);
            simpleBLEState = BLE_STATE_CONNECTED;
            simpleBLEConnHandle = pEvent->linkCmpl.connectionHandle;
            //simpleBLEProcedureInProgress = TRUE;

            // If service discovery not performed initiate service discovery
            if ( simpleBLECharHdl == 0 )
            {
                osal_start_timerEx( simpleBLETaskId, SBP_CENTRAL_DISCOVERY_EVT, 1100 );
            }

            LCD_WRITE_STRING( "Connected", HAL_LCD_LINE_1 );
            LCD_WRITE_STRING( bdAddr2Str( pEvent->linkCmpl.devAddr ), HAL_LCD_LINE_2 );
            
            // 增加从机地址， 注意， 需要连接成功后， 再增加改地址
            //simpleBLE_SetPeripheralMacAddr((uint8 *)(bdAddr2Str( pEvent->linkCmpl.devAddr ))+2);
            //simpleBLE_SetPeripheralMacAddr((uint8 *)bdAddr2Str(simpleBLEDevList[simpleBLEScanIdx].addr) + 2);
            HalLedSet(HAL_LED_3, HAL_LED_MODE_OFF );
            //osal_start_timerEx( simpleBLETaskId, SBP_CENTRAL_SAVE_EVT, 100 );
            //osal_start_timerEx( simpleBLETaskId, SBP_CENTRAL_RSSI_EVT, 10000 );
        }
        else
        {
            simpleBLEState = BLE_STATE_IDLE;
            simpleBLEConnHandle = GAP_CONNHANDLE_INIT;
            //simpleBLERssi = FALSE;
            simpleBLEDiscState = BLE_DISC_STATE_IDLE;
            simpleBLECentralCanSend = FALSE;

            LCD_WRITE_STRING( "Connect Failed", HAL_LCD_LINE_1 );
            LCD_WRITE_STRING_VALUE( "Reason:", pEvent->gap.hdr.status, 10, HAL_LCD_LINE_2 );
            //HalLedSet(HAL_LED_3, HAL_LED_MODE_OFF );   //关LED3
        }
    }
    break;

    case GAP_LINK_TERMINATED_EVENT:
    {
        simpleBLEState = BLE_STATE_IDLE;
        simpleBLEConnHandle = GAP_CONNHANDLE_INIT;
        //simpleBLERssi = FALSE;
        simpleBLEDiscState = BLE_DISC_STATE_IDLE;
        simpleBLECharHdl = 0;
        //simpleBLEProcedureInProgress = FALSE;
        simpleBLECentralCanSend = FALSE;

        LCD_WRITE_STRING( "Disconnected", HAL_LCD_LINE_1 );
        LCD_WRITE_STRING_VALUE( "Reason:", pEvent->linkTerminate.reason,
                                10, HAL_LCD_LINE_2 );
        SerialPrintString("GAP_LINK_TERMINATED_EVENT\r\n");
        //HalLedSet(HAL_LED_3, HAL_LED_MODE_OFF );   //关LED3


        //这里连接失败后， 可以尝试执行重启或者继续扫描从机
        simpleBLEScanRes = 0;
        LCD_WRITE_STRING_VALUE( "simpleBLEScanning=", simpleBLEScanning, 10, HAL_LCD_LINE_1 );
        LCD_WRITE_STRING_VALUE( "simpleBLEScanRes=", simpleBLEScanRes, 10, HAL_LCD_LINE_1 );

        simpleBLEScanning = 0;
        simpleBLEScanRes = 0;
        simpleBLEStartScan();
        //osal_start_timerEx( simpleBLETaskId, SBP_CENTRAL_RSSI_EVT, 500 );
    }
    break;

    case GAP_LINK_PARAM_UPDATE_EVENT:
    {
        SerialPrintString("GAP_LINK_PARAM_UPDATE_EVENT\r\n");
#if 0
        attWriteReq_t AttReq;
        uint8 ValueBuf[2];
        LCD_WRITE_STRING( "Param Update", HAL_LCD_LINE_1 );

        /*
        注意, char6在我们的peripheral中已经被设置为 notify 方式，
        详见 simpleGATTprofile.c 中的以下定义
        // Simple Profile Characteristic 6 Properties
        static uint8 simpleProfileChar6Props = GATT_PROP_READ | GATT_PROP_WRITE_NO_RSP | GATT_PROP_NOTIFY;
        因此， 我们需要对从机的这一特性进行使能， 方可进行后面的通信
        */
        //注意，Enable Notice 的handle值是 对应的特征值的handle+1
        //----------------------amomcu.com--------------------------------------

        AttReq.handle = (simpleBLECharHd6 + 1)/*0x0036*/;
        AttReq.len = 2;
        AttReq.sig = 0;
        AttReq.cmd = 0;
        ValueBuf[0] = 0x01;
        ValueBuf[1] = 0x00;
        osal_memcpy(AttReq.value, ValueBuf, 2);
        GATT_WriteCharValue( 0, &AttReq, simpleBLETaskId );
        LCD_WRITE_STRING( "Enable Notice\n", HAL_LCD_LINE_1);
#endif
#if 0
        SBC_EnableNotification(simpleBLEConnHandle, simpleBLECharHd6 + 1);
        simpleBLECentralCanSend = TRUE;

        //NPI_WriteTransport("Connected\r\n", 11);
        HalUARTWrite(SBP_UART_PORT, (uint8 *)"Connected\r\n", 11);
        SerialPrintValue("simpleBLECharHd6=", simpleBLECharHd6, 10);
#else
        //osal_start_timerEx( simpleBLETaskId, SBP_CENTRAL_DISCOVERY_EVT, 1100 );
        //osal_start_timerEx( simpleBLETaskId, SBP_CENTRAL_EN_NOTIFY_EVT, 1000 );
#endif
    }
    break;

    default:
        break;
    }

    return ( TRUE );
}

bool simpleBLEConnect(uint8 index)
{
    uint8 addrType;
    uint8 *peerAddr;
    uint8 i = index;

    if(i >= simpleBLEScanRes)
        return FALSE;
    if(i >= DEFAULT_MAX_SCAN_RES)
        return FALSE;

    simpleBLEScanIdx = index;
    // connect to current device in scan result
    peerAddr = simpleBLEDevList[i].addr;
    addrType = simpleBLEDevList[i].addrType;

    if(simpleBLEState == BLE_STATE_IDLE)
    {
        simpleBLEState = BLE_STATE_CONNECTING;

        //NPI_WriteTransport("Connecting\r\n", 12);
        HalUARTWrite(SBP_UART_PORT, (uint8 *)"Connecting\r\n", 12);

        LCD_WRITE_STRING( "Connecting", HAL_LCD_LINE_1 );
        LCD_WRITE_STRING( bdAddr2Str( peerAddr ), HAL_LCD_LINE_2 );

        GAPCentralRole_EstablishLink( DEFAULT_LINK_HIGH_DUTY_CYCLE,
                                      DEFAULT_LINK_WHITE_LIST,
                                      addrType, peerAddr );

        return TRUE;
    }
    else
    {
        LCD_WRITE_STRING( "BLE_STATE not in IDLE", HAL_LCD_LINE_1 );
    }
    return FALSE;
}

/*********************************************************************
 * @fn      pairStateCB
 *
 * @brief   Pairing state callback.
 *
 * @return  none
 */
static void simpleBLECentralPairStateCB( uint16 connHandle, uint8 state, uint8 status )
{
    if ( state == GAPBOND_PAIRING_STATE_STARTED )
    {
        LCD_WRITE_STRING( "Pairing started", HAL_LCD_LINE_1 );
    }
    else if ( state == GAPBOND_PAIRING_STATE_COMPLETE )
    {
        if ( status == SUCCESS )
        {
            LCD_WRITE_STRING( "Pairing success", HAL_LCD_LINE_1 );
        }
        else
        {
            LCD_WRITE_STRING_VALUE( "Pairing fail", status, 10, HAL_LCD_LINE_1 );
        }
    }
    else if ( state == GAPBOND_PAIRING_STATE_BONDED )
    {
        if ( status == SUCCESS )
        {
            LCD_WRITE_STRING( "Bonding success", HAL_LCD_LINE_1 );
        }
    }
}

/*********************************************************************
 * @fn      simpleBLECentralPasscodeCB
 *
 * @brief   Passcode callback.
 *
 * @return  none
 */
static void simpleBLECentralPasscodeCB( uint8 *deviceAddr, uint16 connectionHandle,
                                        uint8 uiInputs, uint8 uiOutputs )
{
    uint32  passcode = simpleBle_GetPassword();
#if (defined HAL_LCD) && (HAL_LCD == TRUE)
    uint8   str[7];

    // Create random passcode
    //LL_Rand( ((uint8 *) &passcode), sizeof( uint32 ));
    //passcode %= 1000000;

    // Display passcode to user
    if ( uiOutputs != 0 )
    {
        LCD_WRITE_STRING( "Passcode:",  HAL_LCD_LINE_1 );
        LCD_WRITE_STRING( (char *) _ltoa(passcode, str, 10),  HAL_LCD_LINE_2 );
    }

    LCD_WRITE_STRING_VALUE( ">>>passcode=", passcode, 10, HAL_LCD_LINE_1 );
#endif
    // Send passcode response
    GAPBondMgr_PasscodeRsp( connectionHandle, SUCCESS, passcode );
}

/*********************************************************************
 * @fn      simpleBLECentralStartDiscovery
 *
 * @brief   Start service discovery.
 *
 * @return  none
 */
static void simpleBLECentralStartDiscovery( void )
{
    uint8 uuid[ATT_BT_UUID_SIZE] = { LO_UINT16(SIMPLEPROFILE_SERV_UUID),
                                     HI_UINT16(SIMPLEPROFILE_SERV_UUID)
                                   };
    SerialPrintString("Enter simpleBLECentralStartDiscovery()\r\n");
    // Initialize cached handles
    simpleBLESvcStartHdl = 0;
    simpleBLESvcEndHdl = 0;
    simpleBLECharHdl = 0;

    simpleBLEDiscState = BLE_DISC_STATE_SVC;

    // Discovery simple BLE service
    bStatus_t ret = GATT_DiscPrimaryServiceByUUID( simpleBLEConnHandle,
                                   uuid,
                                   ATT_BT_UUID_SIZE,
                                   simpleBLETaskId );
    SerialPrintValue("ret = ", ret, 10);
}

/*********************************************************************
 * @fn      simpleBLEGATTDiscoveryEvent
 *
 * @brief   Process GATT discovery event
 *
 * @return  none
 */
static void simpleBLEGATTDiscoveryEvent( gattMsgEvent_t *pMsg )
{
    attReadByTypeReq_t req;

    //SerialPrintString("simpleBLEGATTDiscoveryEvent...0\r\n");
    SerialPrintValue("simpleBLEGATTDiscoveryEvent...0, ", pMsg->method, 10);
    if ( simpleBLEDiscState == BLE_DISC_STATE_SVC )
    {
        // Service found, store handles
        if ( pMsg->method == ATT_FIND_BY_TYPE_VALUE_RSP &&
                pMsg->msg.findByTypeValueRsp.numInfo > 0 )
        {
            SerialPrintString("simpleBLEGATTDiscoveryEvent...1\r\n");
            simpleBLESvcStartHdl = ATT_ATTR_HANDLE(pMsg->msg.findByTypeValueRsp.pHandlesInfo, 0);
            simpleBLESvcEndHdl = ATT_GRP_END_HANDLE(pMsg->msg.findByTypeValueRsp.pHandlesInfo, 0);
            SerialPrintValue("simpleBLESvcStartHdl", simpleBLESvcStartHdl, 10);
            SerialPrintValue("simpleBLESvcEndHdl", simpleBLESvcEndHdl, 10);
            // 把每一个handle的值都打印出来
            /*
            for(int i = simpleBLESvcStartHdl; i < simpleBLESvcStartHdl+10; i++)
            {
              //LCD_WRITE_STRING_VALUE( "handle = ", i, 10, HAL_LCD_LINE_1 );
              SerialPrintValue("handle = ", i, 10);
            }
            */
        }

        // If procedure complete
        if ( ( pMsg->method == ATT_FIND_BY_TYPE_VALUE_RSP  &&
                pMsg->hdr.status == bleProcedureComplete ) ||
                ( pMsg->method == ATT_ERROR_RSP ) )
        {
            SerialPrintString("simpleBLEGATTDiscoveryEvent...2\r\n");
            if ( simpleBLESvcStartHdl != 0 )
            {
                // Discover characteristic
                simpleBLEDiscState = BLE_DISC_STATE_CHAR;

                req.startHandle = simpleBLESvcStartHdl;
                req.endHandle = simpleBLESvcEndHdl;
                req.type.len = 2;
                req.type.uuid[0] = LO_UINT16(SIMPLEPROFILE_CHAR6_UUID);
                req.type.uuid[1] = HI_UINT16(SIMPLEPROFILE_CHAR6_UUID);

            #if 1
                GATT_ReadUsingCharUUID( simpleBLEConnHandle, &req, simpleBLETaskId );
            #else
                GATT_DiscCharsByUUID(simpleBLEConnHandle, &req, simpleBLETaskId);
            #endif
            }
        }
    }
    else if ( simpleBLEDiscState == BLE_DISC_STATE_CHAR )
    {
        //SerialPrintString("simpleBLEGATTDiscoveryEvent...3\r\n");
        SerialPrintValue("simpleBLEGATTDiscoveryEvent...3, ", pMsg->method, 10);
        // Characteristic found, store handle
        if ( pMsg->method == ATT_READ_BY_TYPE_RSP &&
                pMsg->msg.readByTypeRsp.numPairs > 0 )
        {
            simpleBLECharHd6 = BUILD_UINT16(pMsg->msg.readByTypeRsp.pDataList[0],
                                            pMsg->msg.readByTypeRsp.pDataList[1]);

            LCD_WRITE_STRING( "Simple Svc Found", HAL_LCD_LINE_1 );
            SerialPrintValue("Simple Svc Found, ", simpleBLECharHd6, 10);
            //simpleBLEProcedureInProgress = FALSE;
            osal_start_timerEx( simpleBLETaskId, SBP_CENTRAL_EN_NOTIFY_EVT, 500 );
            osal_start_timerEx( simpleBLETaskId, SBP_CENTRAL_SAVE_EVT, 2000 );
        }
        else if (pMsg->method == ATT_ERROR_RSP)
        {
            osal_start_timerEx( simpleBLETaskId, SBP_CENTRAL_DISCOVERY_EVT, 1100 );
        }
        simpleBLEDiscState = BLE_DISC_STATE_IDLE;
    }
}


/*********************************************************************
 * @fn      simpleBLEFindSvcUuid
 *
 * @brief   Find a given UUID in an advertiser's service UUID list.
 *
 * @return  TRUE if service UUID found
 */
static bool simpleBLEFindSvcUuid( uint16 uuid, uint8 *pData, uint8 dataLen )
{
    uint8 adLen;
    uint8 adType;
    uint8 *pEnd;

    pEnd = pData + dataLen - 1;

    // While end of data not reached
    while ( pData < pEnd )
    {
        // Get length of next AD item
        adLen = *pData++;
        if ( adLen > 0 )
        {
            adType = *pData;

            // If AD type is for 16-bit service UUID
            if ( adType == GAP_ADTYPE_16BIT_MORE || adType == GAP_ADTYPE_16BIT_COMPLETE )
            {
                pData++;
                adLen--;

                // For each UUID in list
                while ( adLen >= 2 && pData < pEnd )
                {
                    // Check for match
                    if ( pData[0] == LO_UINT16(uuid) && pData[1] == HI_UINT16(uuid) )
                    {
                        // Match found
                        return TRUE;
                    }

                    // Go to next
                    pData += 2;
                    adLen -= 2;
                }

                // Handle possible erroneous extra byte in UUID list
                if ( adLen == 1 )
                {
                    pData++;
                }
            }
            else
            {
                // Go to next item
                pData += adLen;
            }
        }
    }

    // Match not found
    return FALSE;
}

/*********************************************************************
 * @fn      simpleBLEAddDeviceInfo
 *
 * @brief   Add a device to the device discovery result list
 *
 * @return  none
 */
static void simpleBLEAddDeviceInfo( uint8 *pAddr, uint8 addrType )
{
    uint8 i;

    // If result count not at max
    if ( simpleBLEScanRes < DEFAULT_MAX_SCAN_RES )
    {
        // Check if device is already in scan results
        for ( i = 0; i < simpleBLEScanRes; i++ )
        {
            if ( osal_memcmp( pAddr, simpleBLEDevList[i].addr , B_ADDR_LEN ) )
            {
                return;
            }
        }

        // Add addr to scan result list
        osal_memcpy( simpleBLEDevList[simpleBLEScanRes].addr, pAddr, B_ADDR_LEN );
        simpleBLEDevList[simpleBLEScanRes].addrType = addrType;

        // Increment scan result count
        simpleBLEScanRes++;
    }
}

/*********************************************************************
 * @fn      bdAddr2Str
 *
 * @brief   Convert Bluetooth address to string
 *
 * @return  none
 */
static char *bdAddr2Str( uint8 *pAddr )
{
    uint8       i;
    char        hex[] = "0123456789ABCDEF";
    static char str[B_ADDR_STR_LEN];
    char        *pStr = str;

    *pStr++ = '0';
    *pStr++ = 'x';

    // Start from end of addr
    pAddr += B_ADDR_LEN;

    for ( i = B_ADDR_LEN; i > 0; i-- )
    {
        *pStr++ = hex[*--pAddr >> 4];
        *pStr++ = hex[*pAddr & 0x0F];
    }

    *pStr = 0;

    return str;
}

/*********************************************************************
 * @fn      hidappEnableNotification
 *
 * @brief   Enable notification for a given attribute handle.
 *
 * @param   connHandle - connection handle to send notification on
 * @param   attrHandle - attribute handle to send notification for
 *
 * @return  none
 */
static void SBC_EnableNotification( uint16 connHandle, uint16 attrHandle )
{
    attWriteReq_t req;

    req.pValue = GATT_bm_alloc( connHandle, ATT_WRITE_REQ, 2, NULL );
    if ( req.pValue != NULL )
    {
        uint8 notificationsOn[] = {0x01, 0x00};

        req.handle = attrHandle;

        req.len = 2;
        osal_memcpy(req.pValue, notificationsOn, 2);

        req.sig = 0;
        req.cmd = 0;

        if ( GATT_WriteCharValue( connHandle, &req, simpleBLETaskId ) != SUCCESS )
        {
            GATT_bm_free( (gattMsg_t *)&req, ATT_WRITE_REQ );
        }
    }
}
/*********************************************************************
*********************************************************************/
